@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    @if($item->exists)
                        <h1>Edit: {{$item->id}}</h1>
                    @else
                        <h1>Create</h1>
                    @endif

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">admin</a></li>
                        <li class="breadcrumb-item"><a href="#">blog</a></li>
                        <li class="breadcrumb-item"><a href="#">categories</a></li>
                        @if($item->exists)
                            <li class="breadcrumb-item active">Edit: {{$item->id}}</li>
                        @else
                            <li class="breadcrumb-item active">Create</li>
                        @endif
                    </ol>
                </div>
            </div>
        </div>
    </section>

    @include('includes.result_messages')

    <div class="container-fluid">
        @php /** @var \App\Models\BlogCategory $item */ @endphp
        @if($item->exists)
            <form action="{{ route('admin.blog.categories.update', $item->id) }}" method="POST">
                @method('PATCH')
        @else
            <form action="{{ route('admin.blog.categories.store') }}" method="POST">
        @endif
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">Основные данные</h3>
                        </div>

                        <div class="card-body">

                            <div class="form-group">
                                <label for="title">Заголовок</label>
                                <input type="text" name="title" id="title" value="{{$item->title}}"
                                       autocomplete="title" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="slug">Идентификатор</label>
                                <input type="text" name="slug" id="slug" value="{{$item->slug}}"
                                       autocomplete="slug" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="category">Родитель</label>
                                <select id="parent_id" name="parent_id" autocomplete="parent_id"
                                        class="form-control">

                                    @foreach($categoryList as $category)
                                        @if($item->parent_id == $category->id)
                                            <option value="{{$category->id}}" selected>
                                                {{$category->id_title}}
                                            </option>
                                        @else
                                            <option value="{{$category->id}}">{{$category->id_title}}</option>
                                        @endif
                                    @endforeach

                                </select>

                            </div>

                            <div class="form-group">
                                <label>Описание</label>
                                <textarea name="description" id="description"
                                          class="form-control" autocomplete="description"
                                          rows="3">{{ old('description', $item->description) }}</textarea>
                            </div>

                        </div>


                        <div class="card-footer"></div>

                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card card-default">
                        <div class="card-body">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </div>
                    @if($item->exists)
                    <div class="card card-default">
                        <div class="card-body">
                            ID: {{$item->id}}
                        </div>
                    </div>
                    <div class="card card-default">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="created_at">Создано</label>
                                <input for="created_at" type="text" name="created_at" id="created_at"
                                       value="{{$item->created_at}}" autocomplete="created_at"
                                       class="form-control"
                                       disabled="disabled">
                            </div>

                            <div class="form-group">
                                <label for="updated_at">Изменено</label>
                                <input for="updated_at" type="text" name="updated_at" id="updated_at"
                                       value="{{$item->updated_at}}" autocomplete="updated_at"
                                       class="form-control"
                                       disabled="disabled">
                            </div>

                            <div class="form-group">
                                <label for="deleted_at">Удалено</label>
                                <input for="deleted_at" type="text" name="deleted_at" id="deleted_at"
                                       value="{{$item->deleted_at}}" autocomplete="deleted_at"
                                       class="form-control"
                                       disabled="disabled">
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </form>
    </div>
@endsection
