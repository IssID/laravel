@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Categories</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">admin</a></li>
                        <li class="breadcrumb-item"><a href="#">blog</a></li>
                        <li class="breadcrumb-item active">Categories</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-sm-3">
                            <a href="{{ route('admin.blog.categories.create') }}" class="">
                                <button type="button" class="btn btn-block bg-primary">Добавить</button>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-sm-12 col-md-6"></div>
                                <div class="col-sm-12 col-md-6"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example2" class="table table-bordered table-hover dataTable dtr-inline"
                                           role="grid" aria-describedby="example2_info">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting sorting_asc" tabindex="0" aria-controls="example2"
                                                rowspan="1" colspan="1" aria-sort="ascending"
                                                aria-label="Rendering engine: activate to sort column descending">
                                                #
                                            </th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-label="Browser: activate to sort column ascending">
                                                Категория
                                            </th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-label="Platform(s): activate to sort column ascending">
                                                Родитель
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php /** @var \App\Models\BlogCategory $item */  @endphp
                                        @foreach($paginator as $key => $item)
                                            <tr class={{ $key % 2 == 0 ? "even" : "odd" }}>
                                                <td class="dtr-control sorting_1" tabindex="0">{{ $item->id }}</td>
                                                <td>
                                                    <a href="{{route('admin.blog.categories.edit', $item->id)}}">
                                                        {{$item->title}}
                                                    </a>
                                                </td>
                                                <td>
                                                    <span
                                                        @if(in_array($item->parent_id, [1])) class="badge badge-info right" @endif>
                                                        <div class="text-sm text-gray-500">{{optional($item->parentCategory)->title }}</div>
                                                    </span>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th rowspan="1" colspan="1">#</th>
                                            <th rowspan="1" colspan="1">Категория</th>
                                            <th rowspan="1" colspan="1">Родитель</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                @if($paginator->total() > $paginator->count())
                                    <div class="col-sm-12 col-md-5">
                                        <div class="dataTables_info" id="example2_info" role="status"
                                             aria-live="polite">
                                            Showing {{ $paginator->firstItem() }} to {{ $paginator->lastItem() }}
                                            of {{ $paginator->total() }} entries
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-7">
                                        <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                                            {{--pagination--}}
                                            <div class="pagination float-right">
                                                {{ $paginator->links() }}
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
