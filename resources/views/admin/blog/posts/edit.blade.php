@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    @if($item->exists)
                        <h1>Edit: {{$item->id}}</h1>
                    @else
                        <h1>Create</h1>
                    @endif

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">admin</a></li>
                        <li class="breadcrumb-item"><a href="#">blog</a></li>
                        <li class="breadcrumb-item"><a href="#">posts</a></li>
                        @if($item->exists)
                            <li class="breadcrumb-item active">Edit: {{$item->id}}</li>
                        @else
                            <li class="breadcrumb-item active">Create</li>
                        @endif
                    </ol>
                </div>
            </div>
        </div>
    </section>

    @include('includes.result_messages')

    <div class="container-fluid">
        @php /** @var \App\Models\BlogCategory $item */ @endphp
        @if($item->exists)
            <form action="{{ route('admin.blog.posts.update', $item->id) }}" method="POST">
                @method('PATCH')
                @else
                    <form action="{{ route('admin.blog.posts.store') }}" method="POST">
                        @endif
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card card-primary card-outline card-tabs">
                                    <div class="card-header ">
                                        <h3 class="card-title">
                                            @if($item->is_published) Опубликовано @else Не опубликовано @endif
                                        </h3>
                                    </div>
                                    <div class="card-header p-0 pt-1 border-bottom-0">
                                        <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="custom-tabs-three-home-tab"
                                                   data-toggle="pill" href="#custom-tabs-three-home" role="tab"
                                                   aria-controls="custom-tabs-three-home"
                                                   aria-selected="true">Основные данные</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="custom-tabs-three-profile-tab"
                                                   data-toggle="pill" href="#custom-tabs-three-profile" role="tab"
                                                   aria-controls="custom-tabs-three-profile"
                                                   aria-selected="false">Дополнительные данные</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="card-body">
                                        <div class="tab-content" id="custom-tabs-three-tabContent">
                                            <div class="tab-pane fade active show" id="custom-tabs-three-home"
                                                 role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                                                <div class="form-group">
                                                    <label for="title">Заголовок</label>
                                                    <input type="text" name="title" id="title" value="{{$item->title}}"
                                                           autocomplete="title" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label for="content_raw">Статья</label>
                                                    <textarea name="content_raw" id="content_raw"
                                                              class="form-control" autocomplete="content_raw"
                                                              rows="10">{{ old('content_raw', $item->content_raw) }}</textarea>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel"
                                                 aria-labelledby="custom-tabs-three-profile-tab">

                                                <div class="form-group">
                                                    <label for="slug">Идентификатор</label>
                                                    <input type="text" name="slug" id="slug" value="{{$item->slug}}"
                                                           autocomplete="slug" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                    <label for="category_id">Категория</label>
                                                    <select id="category_id" name="category_id" autocomplete="category_id"
                                                            class="form-control">

                                                        @foreach($categoryList as $category)
                                                            @if($item->category_id == $category->id)
                                                                <option value="{{$category->id}}" selected>
                                                                    {{$category->id_title}}
                                                                </option>
                                                            @else
                                                                <option
                                                                    value="{{$category->id}}">{{$category->id_title}}</option>
                                                            @endif
                                                        @endforeach

                                                    </select>

                                                    <div class="form-group">
                                                        <label for="excerpt">Выдержка</label>
                                                        <textarea name="excerpt" id="excerpt"
                                                                  class="form-control" autocomplete="excerpt"
                                                                  rows="4">{{ old('excerpt', $item->excerpt) }}</textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="hidden" name="is_published" value="0">

                                                            <input class="custom-control-input" name="is_published"
                                                                   type="checkbox" id="customCheckbox1"
                                                                   value="1"
                                                                   @if($item->is_published) checked @endif>
                                                            <label for="customCheckbox1" class="custom-control-label">
                                                                Опубликовано
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="card card-default">
                                    <div class="card-body">
                                        <button type="submit" class="btn btn-primary">Сохранить</button>
                                        @if($item->exists)
                                            </form>
                                            <form class="float-right" action="{{ route('admin.blog.posts.destroy', $item->id) }}" method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <button type="submit" class="btn btn-danger right">Удалить</button>
                                            </form>
                                        @endif
                                    </div>
                                </div>
                                @if($item->exists)
                                    <div class="card card-default">
                                        <div class="card-body">
                                            ID: {{$item->id}}
                                        </div>
                                    </div>
                                    <div class="card card-default">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="created_at">Создано</label>
                                                <input for="created_at" type="text" name="created_at" id="created_at"
                                                       value="{{$item->created_at}}" autocomplete="created_at"
                                                       class="form-control"
                                                       disabled="disabled">
                                            </div>

                                            <div class="form-group">
                                                <label for="updated_at">Изменено</label>
                                                <input for="updated_at" type="text" name="updated_at" id="updated_at"
                                                       value="{{$item->updated_at}}" autocomplete="updated_at"
                                                       class="form-control"
                                                       disabled="disabled">
                                            </div>

                                            <div class="form-group">
                                                <label for="deleted_at">Опубликовано</label>
                                                <input for="deleted_at" type="text" name="deleted_at" id="deleted_at"
                                                       value="{{$item->published_at}}" autocomplete="deleted_at"
                                                       class="form-control"
                                                       disabled="disabled">
                                            </div>

                                            <div class="form-group">
                                                <label for="deleted_at">Удалено</label>
                                                <input for="deleted_at" type="text" name="deleted_at" id="deleted_at"
                                                       value="{{$item->deleted_at}}" autocomplete="deleted_at"
                                                       class="form-control"
                                                       disabled="disabled">
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
@endsection
