<?php /** @var \Illuminate\Support\ViewErrorBag $errors */ ?>
@if($errors->any())
    <?php
    $errorText = "<ul>";
    foreach ($errors->all() as $error) {
        $errorText .= "<li>$error</li>";
    }
    $errorText .= "</ul>";

    echo "<script type='text/javascript'>
            document.addEventListener('DOMContentLoaded', function(){
                $(document).Toasts('create', {
                    title: 'Error',
                    body: '" . $errorText . "',
                    class: 'bg-danger',
                    delay: 5000,
                    autohide: true
                })
            });
            </script>";  ?>
@endif

@if(session('success'))
    <?php echo "<script type='text/javascript'>
        document.addEventListener('DOMContentLoaded', function(){
            $(document).Toasts('create', {
                title: 'Success',
                body: '" . session()->get('success') . "',
                class: 'bg-success',
                delay: 5000,
                autohide: true
            })
        });
        </script>";  ?>
@endif
