<!-- need to remove -->
<li class="nav-item">
    <a target="_blank" href="https://adminlte.io/themes/v3/index.html" class="nav-link">
        <i class="nav-icon fas fa-external-link-alt"></i>
        <p>adminlte.io</p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('home') }}" class="nav-link">
        <i class="nav-icon fas fa-home"></i>
        <p>Home</p>
    </a>
</li>

<li class="nav-item">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-book"></i>
        <p>
            Patterns
            <i class="fas fa-angle-left right"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Fundamental</p>
                <i class="right fas fa-angle-left"></i>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('patterns.property-container') }}" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Property container</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('patterns.delegation') }}" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Delegation</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('patterns.event-channel') }}" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Event channel</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('patterns.interface') }}" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Interface Pattern</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Creational</p>
                <i class="right fas fa-angle-left"></i>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('patterns.abstract-factory') }}" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Abstract factory</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('patterns.factory-method') }}" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Factory method</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('patterns.static-factory') }}" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Static factory</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('patterns.simple-factory') }}" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Simple factory</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('patterns.singleton') }}" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Singleton</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('patterns.multiton') }}" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Multiton</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('patterns.builder') }}" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Builder</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('patterns.lazy-initialization') }}" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Lazy initialization</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('patterns.prototype') }}" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Prototype</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('patterns.object-pool') }}" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Object pool</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Structural</p>
                <i class="right fas fa-angle-left"></i>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('patterns.adapter') }}" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Adapter</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('patterns.facade') }}" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Facade</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('patterns.bridge') }}" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Bridge</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('patterns.composite') }}" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Composite</p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Behavioral</p>
                <i class="right fas fa-angle-left"></i>
            </a>
            <ul class="nav nav-treeview" >
                <li class="nav-item">
                    <a href="{{ route('patterns.strategy') }}" class="nav-link">
                        <i class="far fa-dot-circle nav-icon"></i>
                        <p>Strategy</p>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</li>

@auth
    <li class="nav-item">
        <a href="#" class="nav-link">
            <i class="nav-icon fas fa-edit"></i>
            <p>
                Admin
                <i class="fas fa-angle-left right"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{{ route('admin.blog.categories.index') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>categories</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.blog.posts.index') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>posts</p>
                </a>
            </li>
        </ul>
    </li>
@endauth
