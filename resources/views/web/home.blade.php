@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <h1 class="text-black-50">Welcome!</h1>
    </div>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                </div>
            </div>
        </div>
    </section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-black-50">Patterns</h3>
                    </div>
                    <div class="card-body">
                        @foreach($patterns as $key => $pattern)
                            <h4 class="text-black-50">{{$key}}</h4>
                            <ul>
                                @foreach($pattern as $item)
                                    <li><a href="{{$item["url"] ?? ''}}">{{$item["title"] ?? '#'}}</a></li>
                                @endforeach
                            </ul>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-black-50">Info</h3>
                    </div>
                    <div class="card-body">
                        Total patterns: {{$total}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
