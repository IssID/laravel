@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">Шаблон</li>
                        <li class="breadcrumb-item active">{{ $note ?? '' }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="callout callout-info">
                    <h5> {{ $note ?? '' }}</h5>
                    Шаблон проектирования
                </div>

                <div class="card">
                    <div class="card-header">
                        {{ $method ?? '' }}
                    </div>
                    <div class="card-body">
                        <div>
                            <?php echo $description ?? ''; ?>
                        </div>
                        <hr>
                        <div>
                            <?php
                            if (!empty($result)) {
                                \Symfony\Component\VarDumper\VarDumper::dump($result);
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
