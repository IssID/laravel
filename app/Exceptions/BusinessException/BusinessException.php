<?php

namespace App\Exceptions\BusinessException;

class BusinessException extends \Exception
{
    protected string $userMessage;
    protected int $userCode;

    /**
     * @param string $userMessage
     * @param int $userCode
     */
    public function __construct(string $userMessage, int $userCode)
    {
        $this->userMessage = $userMessage;
        $this->userCode = $userCode;
        parent::__construct("Business exception");
    }

    /**
     * @return string
     */
    public function getUserMessage(): string
    {
        return $this->userMessage;
    }

    /**
     * @return int
     */
    public function getUserCode(): int
    {
        return $this->userCode;
    }
}
