<?php

namespace App\Providers;

use App\Models\BlogCategory;
use App\Models\BlogPost;
use App\Models\Patterns\Structural\Adapter\Classes\MediaLibraryAdapter;
use App\Models\Patterns\Structural\Adapter\Classes\MediaLibrarySelfWritten;
use App\Models\Patterns\Structural\Adapter\Interfaces\MediaLibraryInterface;
use App\Observers\BlogCategoryObserver;
use App\Observers\BlogPostObserver;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public $bindings = [
        MediaLibraryInterface::class => MediaLibraryAdapter::class,
//        MediaLibraryInterface::class => MediaLibrarySelfWritten::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();

        BlogPost::observe(BlogPostObserver::class);
        BlogCategory::observe(BlogCategoryObserver::class);
    }
}
