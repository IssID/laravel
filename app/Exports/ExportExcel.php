<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Database\Eloquent\Collection;

class ExportExcel implements FromCollection, WithHeadings
{

    /** @var Collection $collection */
    private $collection = null;

    /**
     * @param Collection $collection
     */
    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
            return $this->collection;
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        $result = [];
        if (isset($this->collection)) {
            foreach ($this->collection->first()->getAttributes() as $attribute => $value) {
                $result[] = $attribute;
            }
        }
        return $result;
    }
}
