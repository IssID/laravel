<?php

namespace App\Dto;

use Carbon\Carbon;

/**
 * @property int $id;
 * @property string $name;
 * @property string $email;
 * @property Carbon $email_verified_at;
 * @property string $password;
 * @property string $remember_token;
 * @property Carbon $created_at;
 * @property Carbon $updated_at;
 */
class UserDto
{
    public ?int $id;
    public ?string $name;
    public ?string $email;
    public ?Carbon $email_verified_at;
    public ?string $password;
    public ?string $remember_token;
    public ?Carbon $created_at;
    public ?Carbon $updated_at;
}
