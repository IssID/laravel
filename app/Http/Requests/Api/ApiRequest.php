<?php

namespace App\Http\Requests\Api;

use App\Traits\ApiResponse;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

abstract class ApiRequest extends FormRequest
{
    use ApiResponse;

    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->error(
            $validator->getMessageBag()->first(),
            400,
            []
        ));
    }

    /**
     * @return void
     */
    protected function prepareForValidation()
    {
        if ($id = $this->route('key')) {
            $this->merge([
                'id' => $id,
            ]);
        }
    }
}
