<?php

namespace App\Http\Requests\Api;

class SendMailRequest extends ApiRequest
{
    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return string[]
     */
    public function rules()
    {
        return [
            'email' => 'required|email|min:5|max:200',
        ];
    }
}
