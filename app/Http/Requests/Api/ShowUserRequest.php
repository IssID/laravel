<?php

namespace App\Http\Requests\Api;

class ShowUserRequest extends ApiRequest
{
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return string[]
     */
    public function rules()
    {
        return [
            'id' => 'required|integer',
            'name' => 'string',
            'email' => 'string',
            'email_verified_at' => 'string',
            'created_at' => 'string',
            'updated_at' => 'string',
        ];
    }
}

