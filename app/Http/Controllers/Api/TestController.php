<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\BusinessException\UserNotFoundException;
use App\Factories\CreateUserDtoFactory;
use App\Http\Requests\Api\SendMailRequest;
use App\Http\Requests\Api\ShowUserRequest;
use App\Jobs\SendMailJob;
use App\Services\SendMailService;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;

class TestController extends ApiController
{
    /**
     * Send test email
     *
     * @param SendMailRequest $request
     * @param SendMailService $service
     * @return JsonResponse|mixed
     */
    public function sendMailTest(SendMailRequest $request, SendMailService $service)
    {
//        try {
//            $service->sendEmail($request->getEmail());
//        } catch (MailException $exception) {
//            Log::error(now() . $exception->getMessage() . $exception->getTraceAsString());
//            return $this->error($exception->getMessage(), 500);
//        }


        $mail = $service->getMail($request->getEmail());
        $job = new SendMailJob($mail);
        $this->dispatch($job);

        return $this->success();
    }

    /**
     * Get User By Id
     *
     * @param ShowUserRequest $request
     * @param UserService $service
     * @return JsonResponse|mixed
     * @throws UserNotFoundException
     */
    public function getUser(ShowUserRequest $request, UserService $service)
    {
        $userDto = CreateUserDtoFactory::fromRequest($request);
        $user = $service->getUserById($userDto);

        return $this->success($user);
    }
}
