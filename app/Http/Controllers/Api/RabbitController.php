<?php

namespace App\Http\Controllers\Api;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitController extends ApiController
{
    public function sending()
    {
        $connection = new AMQPStreamConnection('rabbitmq', 5672, env("RABBITMQ_USER"), env("RABBITMQ_PASSWORD"));
        $channel = $connection->channel();

        $channel->queue_declare('hello', false, false, false, false);
        $msg = new AMQPMessage('Hello World!');
        $channel->basic_publish($msg, '', 'hello');
        echo " [x] Sent 'Hello World!'\n";
        $channel->close();
        $connection->close();
        dd("end");
    }

    public function receiving()
    {
        $connection = new AMQPStreamConnection('rabbitmq', 5672, env("RABBITMQ_USER"), env("RABBITMQ_PASSWORD"));
        $channel = $connection->channel();

        $channel->queue_declare('hello', false, false, false, false);

        $callback = function ($msg) {
            echo ' [x] Received ', $msg->body, "\n";
        };

        $channel->basic_consume('hello', '', false, true, false, false, $callback);

        try {
            $channel->consume();
        } catch (\Throwable $exception) {
            echo $exception->getMessage();
        }
        dd("end");
    }
}
