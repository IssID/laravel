<?php

namespace App\Http\Controllers\Web\Patterns;

use App\Models\Patterns\Fundamental\Delegation\AppMessenger;
use App\Models\Patterns\Fundamental\EventChannel\EventChannel;
use App\Models\Patterns\Fundamental\EventChannel\EventChannelJob;
use App\Models\Patterns\Fundamental\EventChannel\Publisher;
use App\Models\Patterns\Fundamental\EventChannel\Subscriber;
use App\Models\Patterns\Fundamental\PropertyContainer\BlogPost;
use Illuminate\Contracts\View\View;

/**
 * Бизнес логика в контроллере для лучшего понимания паттерна
 */
class FundamentalPatternsController extends BaseController
{
    /**
     * @return View
     * @throws \Exception
     */
    public function PropertyContainer()
    {
        $note = 'Контейнер свойств'; $method = __METHOD__; $result = [];
        $description = 'Контейнер свойств (англ. property container) — фундаментальный шаблон проектирования,
        который служит для обеспечения возможности уже построенного и развернутого приложения <br><br>
        <a href="https://ru.wikipedia.org/wiki/Контейнер_свойств_(шаблон_проектирования)" target="_blank">
        https://ru.wikipedia.org/wiki/Контейнер_свойств_(шаблон_проектирования)</a>';

        $item = new BlogPost();

        $item->setTitle("Заголовок статьи");
        $item->setCategory(10);

        $item->addProperty('view_count', 100);

        $item->addProperty('last_update', '2020-02-01');
        $item->setProperty('last_update', '2020-02-02');

        $item->addProperty('read_only', true);
        $item->deleteProperty('read_only');

        $result = $item;
        return view('patterns.index' , compact('note', 'method', 'description', 'result'));
    }

    /**
     * @return View
     */
    public function Delegation()
    {
        $note = 'Делегирование '; $method = __METHOD__; $result = [];
        $description = 'Делегирование (англ. Delegation) — основной шаблон проектирования, в котором объект внешне
        выражает некоторое поведение, но в реальности передаёт ответственность за выполнение этого поведения связанному
        объекту. Шаблон делегирования является фундаментальной абстракцией, на основе которой реализованы другие
        шаблоны - композиция (также называемая агрегацией), примеси (mixins) и аспекты (aspects). <br><br>
        <a href="https://ru.wikipedia.org/wiki/Шаблон_делегирования" target="_blank">
        https://ru.wikipedia.org/wiki/Шаблон_делегирования</a>';

        $item = new AppMessenger();

        $item->setSender('sender@test.test')
            ->setRecipient('recipient@test.test')
            ->setMessage('test email message')
            ->send();

        \Debugbar::info($item);
        $item->toSms()
            ->setSender(70123456789)
            ->setRecipient(79876543210)
            ->setMessage('test sms message')
            ->send();

        \Debugbar::info($item);



        return view('patterns.index' , compact('note', 'method', 'description', 'result'));
    }

    /**
     * @return View
     */
    public function EventChannel()
    {
        $note = 'Канал событий'; $method = __METHOD__; $result = [];
        $description = 'Канал событий (англ. event channel) — фундаментальный шаблон проектирования, используется для
        создания канала связи и коммуникации через него посредством событий. Этот канал обеспечивает возможность разным
        издателям публиковать события и подписчикам, подписываясь на них, получать уведомления.
        <br><br>
        <a href="https://ru.wikipedia.org/wiki/Канал_событий_(шаблон_проектирования)" target="_blank">
        https://ru.wikipedia.org/wiki/Канал_событий_(шаблон_проектирования)</a>';

        $newsChannel = new EventChannel();

        // Создается поставщик с названием канала
        $highgardenGroup = new Publisher('highgarden-news', $newsChannel);
        $winterfellNews = new Publisher('winterfell-news', $newsChannel);
        $winterfellDaily = new Publisher('winterfell-news', $newsChannel);

        // Создание подписчика
        $sansa = new Subscriber('Sansa Stark');
        $arya = new Subscriber('Arya Stark');
        $cersei = new Subscriber('Sersei Lannister');
        $snow = new Subscriber('Jon Snow');

        // Подписчики подписались на канал
        $newsChannel->subscribe('highgarden-news', $cersei);
        $newsChannel->subscribe('winterfell-news', $sansa);
        $newsChannel->subscribe('highgarden-news', $arya);
        $newsChannel->subscribe('winterfell-news', $arya);
        $newsChannel->subscribe('winterfell-news', $snow);

        // Поставщик публикует данные
        $highgardenGroup->publish("New highgarden post");
        $winterfellNews->publish("New winterfell post");
        $winterfellDaily->publish("Alternative winterfell post");

        return view('patterns.index' , compact('note', 'method', 'description', 'result'));
    }

    /**
     * @return View
     */
    public function InterfacePattern()
    {
        $note = 'Шаблон интерфейс'; $method = __METHOD__; $result = [];
        $description = 'Интерфейс (англ. interface) — основной шаблон проектирования, являющийся общим методом для
        структурирования компьютерных программ для того, чтобы их было проще понять. В общем, интерфейс — это класс,
        который обеспечивает программисту простой или более программно-специфический способ доступа к другим классам.
        <br><br>
        Интерфейс может содержать набор объектов и обеспечивать простую, высокоуровневую функциональность для
        программиста (например, Шаблон Фасад); он может обеспечивать более чистый или более специфический способ
        использования сложных классов («класс-обёртка»); он может использоваться в качестве «клея» между двумя
        различными API (Шаблон Адаптер); и для многих других целей.
        <br><br>
        Другими типами интерфейсных шаблонов являются: Шаблон делегирования, Шаблон компоновщик, и Шаблон мост.
        <br><br>
        <a href="https://ru.wikipedia.org/wiki/Интерфейс_(шаблон_проектирования)" target="_blank">
        https://ru.wikipedia.org/wiki/Интерфейс_(шаблон_проектирования)</a>';

        $item = new EventChannelJob();
        // Реализация интерфейса
        $item->run();

        return view('patterns.index' , compact('note', 'method', 'description', 'result'));
    }
}
