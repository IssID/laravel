<?php

namespace App\Http\Controllers\Web\Patterns;

use App\Models\Patterns\Structural\Adapter\Classes\MediaLibraryAdapter;
use App\Models\Patterns\Structural\Adapter\Classes\MediaLibrarySelfWritten;
use App\Models\Patterns\Structural\Adapter\Interfaces\MediaLibraryInterface;
use App\Models\Patterns\Structural\Bridge\BridgeDemo;
use App\Models\Patterns\Structural\Facade\ComputerFacade;
use Illuminate\Contracts\View\View;

/**
 * Бизнес логика в контроллере для лучшего понимания паттерна
 */
class StructuralPatternsController
{
    /**
     * @return View
     */
    public function Adapter()
    {
        $note = 'Адаптер (Adapter)'; $method = __METHOD__; $result = [];
        $description = 'Структурный шаблон проектирования, предназначенный для организации использования функций
        объекта, недоступного для модификации, через специально созданный интерфейс. Другими словами — это структурный
        паттерн проектирования, который позволяет объектам с несовместимыми интерфейсами работать вместе.
        <br><br>
        <a href="https://ru.wikipedia.org/wiki/Адаптер_(шаблон_проектирования)" target="_blank">
        https://ru.wikipedia.org/wiki/Адаптер_(шаблон_проектирования)</a>';

        $mediaLibrary = app(MediaLibrarySelfWritten::class);
        $result[] = $mediaLibrary->upload('ImageFile');
        $result[] = $mediaLibrary->get('ImageFile');

        $mediaLibrary = app(MediaLibraryAdapter::class);
        $result[] = $mediaLibrary->upload('ImageFile');
        $result[] = $mediaLibrary->get('ImageFile');
        $result[] = $mediaLibrary->newMethod();

        // laravel $binding App\Providers\AppServiceProvider
        $mediaLibrary = app(MediaLibraryInterface::class);
        $result[] = $mediaLibrary->upload('ImageFile');
        $result[] = $mediaLibrary->get('ImageFile');

        return view('patterns.index' , compact('note', 'method', 'description', 'result'));
    }

    /**
     * @return View
     */
    public function Facade()
    {
        $note = 'Фасад (Facade)'; $method = __METHOD__; $result = [];
        $description = 'Структурный шаблон проектирования, позволяющий скрыть сложность системы путём сведения всех
        возможных внешних вызовов к одному объекту, делегирующему их соответствующим объектам системы.
        <br><br>
        <a href="https://ru.wikipedia.org/wiki/Фасад_(шаблон_проектирования)" target="_blank">
        https://ru.wikipedia.org/wiki/Фасад_(шаблон_проектирования)</a>';

        $computer = new ComputerFacade();
        $computer->startComputer();

        return view('patterns.index' , compact('note', 'method', 'description', 'result'));
    }

    /**
     * @return View
     */
    public function Bridge()
    {
        $note = 'Мост (Bridge)'; $method = __METHOD__; $result = [];
        $description = 'Структурный шаблон проектирования, используемый в проектировании программного обеспечения чтобы
        «разделять абстракцию и реализацию так, чтобы они могли изменяться независимо». Шаблон мост использует
        инкапсуляцию, агрегирование и может использовать наследование для того, чтобы разделить ответственность
        между классами.
        <br><br>
        <a href="https://ru.wikipedia.org/wiki/Мост_(шаблон_проектирования)" target="_blank">
        https://ru.wikipedia.org/wiki/Мост_(шаблон_проектирования)</a>';

//        (new WithoutBridgeDemo())->run();
        (new BridgeDemo())->run();

        return view('patterns.index' , compact('note', 'method', 'description', 'result'));
    }

    /**
     * @return View
     */
    public function Composite()
    {
        $note = 'Компоновщик (Composite)'; $method = __METHOD__; $result = [];
        $description = 'Структурный шаблон проектирования, объединяющий объекты в древовидную структуру для
        представления иерархии от частного к целому. Компоновщик позволяет клиентам обращаться к отдельным объектам
        и к группам объектов одинаково.
        <br><br>
        <a href="https://ru.wikipedia.org/wiki/Компоновщик_(шаблон_проектирования)" target="_blank">
        https://ru.wikipedia.org/wiki/Компоновщик_(шаблон_проектирования)</a>';


        return view('patterns.index' , compact('note', 'method', 'description', 'result'));
    }
}
