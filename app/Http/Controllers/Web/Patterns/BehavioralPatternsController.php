<?php

namespace App\Http\Controllers\Web\Patterns;

use App\Models\Patterns\Behavioral\Strategy\SalaryManager;
use App\Models\User;
use Carbon\Carbon;

/**
 * Бизнес логика в контроллере для лучшего понимания паттерна
 */
class BehavioralPatternsController
{
    public function Strategy()
    {
        $note = 'Стратегия'; $method = __METHOD__; $result = [];
        $description = 'Стратегия (англ. Strategy) — поведенческий шаблон проектирования, предназначенный для
        определения семейства алгоритмов, инкапсуляции каждого из них и обеспечения их взаимозаменяемости. Это
        позволяет выбирать алгоритм путём определения соответствующего класса. Шаблон Strategy позволяет менять
        выбранный алгоритм независимо от объектов-клиентов, которые его используют.
        <br><br>
        <a href="https://ru.wikipedia.org/wiki/Стратегия_(шаблон_проектирования)" target="_blank">
        https://ru.wikipedia.org/wiki/Стратегия_(шаблон_проектирования)</a>
        <hr>
        Стратегия — это поведенческий паттерн, выносит набор алгоритмов в собственные классы и делает их взаимозаменимыми.
        <br><br>
        <a href="https://refactoring.guru/ru/design-patterns/strategy/php/example" target="_blank">
        https://refactoring.guru/ru/design-patterns/strategy/php/example</a>';

        $period = [
            Carbon::now()->subMonth()->startOfMonth(),
            Carbon::now()->subMonth()->endOfMonth()
        ];
        $users = User::all();

        $result = (new SalaryManager($period, $users))->handle();

        return view('patterns.index' , compact('note', 'method', 'description', 'result'));
    }
}
