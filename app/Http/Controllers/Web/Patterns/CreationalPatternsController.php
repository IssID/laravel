<?php

namespace App\Http\Controllers\Web\Patterns;

use App\Models\Patterns\Creational\AbstractFactory\GuiKitFactory;
use App\Models\Patterns\Creational\Builder\BlogPostBuilder;
use App\Models\Patterns\Creational\Builder\BlogPostDirector;
use App\Models\Patterns\Creational\FactoryMethod\BootstrapDialogForm;
use App\Models\Patterns\Creational\FactoryMethod\SemanticUiDialogForm;
use App\Models\Patterns\Creational\LazyInitialization\LazyInitialization;
use App\Models\Patterns\Creational\Multiton\SimpleMultiton;
use App\Models\Patterns\Creational\Multiton\SimpleMultitonNext;
use App\Models\Patterns\Creational\ObjectPool\ObjectPoolDemo;
use App\Models\Patterns\Creational\Prototype\PrototypeDemo;
use App\Models\Patterns\Creational\SimpleFactory\MessageSumpleFactory;
use App\Models\Patterns\Creational\Singleton\AdvancedSingleton;
use App\Models\Patterns\Creational\Singleton\SimpleSingleton;
use App\Models\Patterns\Creational\StaticFactory\StaticFactory;
use Illuminate\Contracts\View\View;

/**
 * Бизнес логика в контроллере для лучшего понимания паттерна
 */
class CreationalPatternsController
{
    /**
     * @return View
     * @throws \Exception
     */
    public function AbstractFactory()
    {
        $note = 'Абстрактная фабрика'; $method = __METHOD__; $result = [];
        $description = 'Абстрактная фабрика (англ. Abstract factory) — порождающий шаблон проектирования, предоставляет
        интерфейс для создания семейств взаимосвязанных или взаимозависимых объектов, не специфицируя их конкретных
        классов. Шаблон реализуется созданием абстрактного класса Factory, который представляет собой интерфейс для
        создания компонентов системы (например, для оконного интерфейса он может создавать окна и кнопки). Затем пишутся
        классы, реализующие этот интерфейс.
        <br><br>
        <a href="https://ru.wikipedia.org/wiki/Абстрактная_фабрика_(шаблон_проектирования)" target="_blank">
        https://ru.wikipedia.org/wiki/Абстрактная_фабрика_(шаблон_проектирования)</a>
        <br><br>
        <a href="https://refactoring.guru/ru/design-patterns/abstract-factory" target="_blank">
        https://refactoring.guru/ru/design-patterns/abstract-factory</a>';


        $guiKitfactory = new GuiKitFactory();
        $bootstrapFactory = $guiKitfactory->getFactory('bootstrap');
        $result['bootstrap'][] = $bootstrapFactory->buildButton()->draw();
        $result['bootstrap'][] = $bootstrapFactory->buildCheckBox()->draw();

        $semanticFactory = $guiKitfactory->getFactory('semantic');
        $result['semantic'][] = $semanticFactory->buildButton()->draw();
        $result['semantic'][] = $semanticFactory->buildCheckBox()->draw();

        return view('patterns.index' , compact('note', 'method', 'description', 'result'));
    }

    /**
     * @return View
     */
    public function FactoryMethod()
    {
        $note = 'Фабричный метод'; $method = __METHOD__; $result = [];
        $description = 'Фабричный метод (англ. Factory Method), или виртуальный конструктор (англ. Virtual Constructor)
        — порождающий шаблон проектирования, предоставляющий подклассам (дочерним классам) интерфейс для создания
        экземпляров некоторого класса. В момент создания наследники могут определить, какой класс создавать. Иными
        словами, данный шаблон делегирует создание объектов наследникам родительского класса. Это позволяет использовать
        в коде программы не конкретные классы, а манипулировать абстрактными объектами на более высоком уровне.
        <br><br>
        <a href="https://ru.wikipedia.org/wiki/Фабричный_метод_(шаблон_проектирования)" target="_blank">
        https://ru.wikipedia.org/wiki/Фабричный_метод_(шаблон_проектирования)</a>';

        $dialogForm = new BootstrapDialogForm();
        $result[] = $dialogForm->render();

        $dialogForm = new SemanticUiDialogForm();
        $result[] = $dialogForm->render();

        return view('patterns.index' , compact('note', 'method', 'description', 'result'));
    }

    /**
     * @return View
     * @throws \Exception
     */
    public function StaticFactory()
    {
        $note = 'Статическая фабрика'; $method = __METHOD__; $result = [];
        $description = 'Разница между этим и шаблоном и простой фабрикой заключается в том, что Статическая фабрика
        использует только один статический метод, чтобы создать все допустимые типы объектов.
        Этот метод, обычно, называется factory или build
        <hr>
        Подобно AbstractFactory, этот паттерн используется для создания ряда связанных или зависимых объектов.
        Разница между этими шаблонами  и Абстрактной фабрикой заключается в том, что Статическая фабрика использует
        только один статический метод, чтобы создать все допустимые типы объектов.
        <hr>
        Статический фабричный метод — вариация создающего метода, объявленная как static. Если этот метод создаёт
        объекты своего же класса, то, по сути, он выступает в роли альтернативного конструктора.
        <a href="https://refactoring.guru/ru/design-patterns/factory-comparison" target="_blank">
        https://refactoring.guru/ru/design-patterns/factory-comparison</a>';

        $appMailMessenger = StaticFactory::build('email');
        $appSmsMessenger = StaticFactory::build('sms');
        $result = [$appMailMessenger, $appSmsMessenger];

        return view('patterns.index' , compact('note', 'method', 'description', 'result'));
    }

    /**
     * @return View
     * @throws \Exception
     */
    public function SimpleFactory()
    {
        $note = 'Простая фабрика'; $method = __METHOD__; $result = [];
        $description = 'Паттерн Простая фабрика  — это класс, в котором есть один метод с большим условным оператором,
        выбирающим создаваемый продукт. Этот метод вызывают с неким параметром, по которому определяется какой из
        продуктов нужно создать. У простой фабрики, обычно, нет подклассов.
        <br><br>
        <a href="https://refactoring.guru/ru/design-patterns/factory-comparison" target="_blank">
        https://refactoring.guru/ru/design-patterns/factory-comparison</a>';

        $factory = new MessageSumpleFactory();
        $appMailMessenger = $factory->build('email');
        $appSmsMessenger = $factory->build('sms');

        $result = [$appMailMessenger, $appSmsMessenger];

        return view('patterns.index' , compact('note', 'method', 'description', 'result'));
    }

    /**
     * @return View
     */
    public function Singleton()
    {
        $note = 'Одиночка (Singleton)'; $method = __METHOD__; $result = [];
        $description = 'Одиночка (англ. Singleton) — порождающий шаблон проектирования, гарантирующий, что в
        однопоточном приложении будет единственный экземпляр некоторого класса, и предоставляющий глобальную точку
        доступа к этому экземпляру.
        <br><br>
        <a href="https://ru.wikipedia.org/wiki/Одиночка_(шаблон_проектирования)" target="_blank">
        https://ru.wikipedia.org/wiki/Одиночка_(шаблон_проектирования)</a>';

        $result['simpleSingleton']['first'] = SimpleSingleton::getInstance();
        $result['simpleSingleton']['first']->setTest("simpleSingleton first");
        $result['simpleSingleton']['second'] = SimpleSingleton::getInstance();
        $result['simpleSingleton']['equal'] = $result['simpleSingleton']['first'] === $result['simpleSingleton']['second'];

        $result['AdvancedSingleton']['first'] = AdvancedSingleton::getInstance();
        $result['AdvancedSingleton']['first']->setTest("AdvancedSingleton first");
        $result['AdvancedSingleton']['second'] = AdvancedSingleton::getInstance();
        $result['AdvancedSingleton']['equal'] = $result['AdvancedSingleton']['first'] === $result['AdvancedSingleton']['second'];

        return view('patterns.index' , compact('note', 'method', 'description', 'result'));
    }

    /**
     * @return View
     */
    public function Multiton()
    {
        $note = 'Мультитон (Multiton)'; $method = __METHOD__; $result = [];
        $description = 'Мультитон (англ. multiton) — порождающий шаблон проектирования, который обобщает шаблон
        "Одиночка". В то время, как "Одиночка" разрешает создание лишь одного экземпляра класса, мультитон позволяет
        создавать несколько экземпляров, которые управляются через ассоциативный массив. Создаётся лишь один экземпляр
        для каждого из ключей ассоциативного массива, что позволяет контролировать уникальность объекта по какому-либо
        признаку.
        <br><br>
        <a href="https://ru.wikipedia.org/wiki/Мультитон_(шаблон_проектирования)" target="_blank">
        https://ru.wikipedia.org/wiki/Мультитон_(шаблон_проектирования)</a>';

        $result['mysql'] = SimpleMultiton::getInstance('mysql');
        $result['mysql']->setTest('mysql-test');
        $result['mongo'] = SimpleMultiton::getInstance('mongo');

        $result['mysql2'] = SimpleMultiton::getInstance('mysql');
        $result['mongo2'] = SimpleMultiton::getInstance('mongo');
        $result['mongo2']->setTest('mongo-test');

        $simpleMultitonNext = SimpleMultitonNext::getInstance('mysql');
        $simpleMultitonNext->test2 = 'init';
        $result['mysqlNext'] = $simpleMultitonNext;

        $simpleMultitonNext = SimpleMultitonNext::getInstance('mysql');
        $simpleMultitonNext->test2 = 'init2';
        $result['mysqlNext2'] = $simpleMultitonNext;


        return view('patterns.index' , compact('note', 'method', 'description', 'result'));
    }

    /**
     * @return View
     */
    public function Builder()
    {
        $note = 'Строитель (Builder)'; $method = __METHOD__; $result = [];
        $description = 'Строитель (англ. Builder) — порождающий шаблон проектирования предоставляет способ создания
        составного объекта.
        <br><br>
        <a href="https://ru.wikipedia.org/wiki/Строитель_(шаблон_проектирования)" target="_blank">
        https://ru.wikipedia.org/wiki/Строитель_(шаблон_проектирования)</a>';

        $builder = new BlogPostBuilder();
        $posts[] = $builder->setTitle('form Builder')->getBlogPost();

        $director =  new BlogPostDirector();
        $director->setBuilder($builder);

        $posts[] = $director->createCleanPost();
        $posts[] = $director->createNewPostIt();
        $posts[] = $director->createNewPostCats();

        $result = $posts;
        return view('patterns.index' , compact('note', 'method', 'description', 'result'));
    }

    /**
     * @return View
     */
    public function LazyInitialization()
    {
        $note = 'Отложенная инициализация (Lazy Initialization)'; $method = __METHOD__; $result = [];
        $description = 'Отложенная (ленивая) инициализация (англ. Lazy initialization) — приём в программировании,
        когда некоторая ресурсоёмкая операция (создание объекта, вычисление значения) выполняется непосредственно перед
        тем, как будет использован её результат. Таким образом, инициализация выполняется «по требованию», а не
        заблаговременно. Аналогичная идея находит применение в самых разных областях: например, компиляция «на лету»
        и логистическая концепция «Точно в срок».

        Частный случай ленивой инициализации — создание объекта в момент обращения к нему — является одним из
        порождающих шаблонов проектирования. Как правило, он используется в сочетании с такими шаблонами как
        Фабричный метод, Одиночка и Заместитель.
        <br><br>
        <a href="https://ru.wikipedia.org/wiki/Отложенная_инициализация" target="_blank">
        https://ru.wikipedia.org/wiki/Отложенная_инициализация</a>';

        $lazyLoad = new LazyInitialization();

        $result[] = $lazyLoad->getUser()->name;
        $result[] = $lazyLoad->getUser()->email;
        $result[] = $lazyLoad->getUser()->created_at;

        return view('patterns.index' , compact('note', 'method', 'description', 'result'));
    }

    /**
     * @return View
     */
    public function Prototype()
    {
        $note = 'Прототип (Prototype)'; $method = __METHOD__; $result = [];
        $description = 'Задаёт виды создаваемых объектов с помощью экземпляра-прототипа и создаёт новые объекты путём
        копирования этого прототипа. Он позволяет уйти от реализации и позволяет следовать принципу «программирование
        через интерфейсы». В качестве возвращающего типа указывается интерфейс/абстрактный класс на вершине иерархии,
        а классы-наследники могут подставить туда наследника, реализующего этот тип.
        <br><br>
        <a href="https://ru.wikipedia.org/wiki/Прототип_(шаблон_проектирования)" target="_blank">
        https://ru.wikipedia.org/wiki/Прототип_(шаблон_проектирования)</a>';

        $prototype = new PrototypeDemo();
        $result = $prototype->run();

        return view('patterns.index' , compact('note', 'method', 'description', 'result'));
    }


    /**
     * @return View
     * @throws \Exception
     */
    public function ObjectPool()
    {
        $note = 'Объектный пул (object pool)'; $method = __METHOD__; $result = [];
        $description = 'Порождающий шаблон проектирования, набор инициализированных и готовых к использованию объектов.
        Когда системе требуется объект, он не создаётся, а берётся из пула. Когда объект больше не нужен, он не
        уничтожается, а возвращается в пул.
        <br><br>
        <a href="https://ru.wikipedia.org/wiki/Объектный_пул" target="_blank">
        https://ru.wikipedia.org/wiki/Объектный_пул</a>';

        $objectPool = new ObjectPoolDemo();
        $result = $objectPool->run();

        return view('patterns.index' , compact('note', 'method', 'description', 'result'));
    }
}
