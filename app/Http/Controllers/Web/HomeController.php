<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Services\HomeService;
use Illuminate\Contracts\Support\Renderable;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index(HomeService $service)
    {
        $patterns = $service::getList();
        $total = $service::getTotal();
        return view('web.home', compact('patterns', 'total'));
    }
}
