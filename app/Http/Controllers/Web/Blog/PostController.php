<?php

namespace App\Http\Controllers\Web\Blog;

use App\Models\BlogPost;
use Illuminate\Contracts\View\View;

class PostController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $items = BlogPost::query()->paginate(25);
        return view('web.blog.posts.index', compact('items'));
    }
}
