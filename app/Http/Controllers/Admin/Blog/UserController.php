<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Exports\ExportExcel;
use App\Services\UserService;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Exception;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class UserController extends BaseController
{
    /**
     * Download excel file
     *
     * @return BinaryFileResponse
     * @throws Exception
     */
    public function excel(UserService $service)
    {
        $users = $service->getUsersForExcel();
        $fileName = 'Users_' . now()->format('Y-m-d') . '.xlsx';
        return Excel::download(new ExportExcel($users), $fileName);
    }
}

