<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Http\Requests\Blog\BlogCategoryCreateRequest;
use App\Http\Requests\Blog\BlogCategoryUpdateRequest;
use App\Models\BlogCategory;
use App\Repositories\Admin\BlogCategoryRepository;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

/**
 * Управление категориями блога
 *
 * @package App\Http\Controllers\Blog\Admin
 */
class CategoryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(BlogCategoryRepository $blogCategoryRepository)
    {
        $paginator = $blogCategoryRepository->getAllWithPaginate(5);
        return view('admin.blog.categories.index', compact('paginator'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(BlogCategoryRepository $blogCategoryRepository)
    {
        $item = new BlogCategory();
        $categoryList = $blogCategoryRepository->getForComboBox();
        return view('admin.blog.categories.create', compact('item', 'categoryList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BlogCategoryCreateRequest $request
     * @return RedirectResponse
     */
    public function store(BlogCategoryCreateRequest $request)
    {
        $data = $request->input();

        if ($item = (new BlogCategory())->query()->create($data)) {
            return redirect()
                ->route('admin.blog.categories.edit', $item->id)
                ->with(['success' => 'Успешно Сохранен.']);
        } else {
            return back()
                ->withErrors(['msg' => "Ошибка сохранения."])
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function show($id)
    {
        dd(__METHOD__);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @param BlogCategoryRepository $blogCategoryRepository
     * @return View
     */
    public function edit(int $id, BlogCategoryRepository $blogCategoryRepository)
    {
        if (!$item = $blogCategoryRepository->getEdit($id)) {
            abort(404);
        }
        if (!$categoryList = $blogCategoryRepository->getForComboBox()) {
            abort(404);
        }

        return view('admin.blog.categories.edit', compact('item', 'categoryList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BlogCategoryUpdateRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(int $id, BlogCategoryUpdateRequest $request, BlogCategoryRepository $blogCategoryRepository)
    {
        if (empty($item = $blogCategoryRepository->getEdit($id))) {
            return back()->withErrors(['msg' => "Запись id=[{$id}] не найдена."])->withInput();
        }

        $data = $request->all();

        if ($item->update($data)) {
            return redirect()->route('admin.blog.categories.edit', $item->id)->with(['success' => 'Успешно Сохранен.']);
        } else {
            return back()->withErrors(['msg' => "Ошибка сохранения."])->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id)
    {
        // soft delete
        $result = BlogCategory::destroy($id);
        // full delete
//        $result = BlogCategory::query()->find($id)->forceDelete();

        if ($result) {
            return redirect()->route('admin.blog.posts.index')->with(['success' => "Запись [$id] удалена"]);
        } else {
            return back()->withErrors(['msg' => "Ошибка удаления."])->withInput();
        }
    }
}
