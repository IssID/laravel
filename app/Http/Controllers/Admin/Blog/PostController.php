<?php


namespace App\Http\Controllers\Admin\Blog;

use App\Http\Requests\Blog\BlogPostCreateRequest;
use App\Http\Requests\Blog\BlogPostUpdateRequest;
use App\Jobs\BlogPostAfterCreateJob;
use App\Jobs\BlogPostAfterDeleteJob;
use App\Models\BlogPost;
use App\Repositories\Admin\BlogCategoryRepository;
use App\Repositories\Admin\BlogPostRepository;
use Illuminate\Http\RedirectResponse;

/**
 * Blog Articles Management
 * @package App\Http\Controllers\Blog\Admin
 */
class PostController extends BaseController
{
    /**
     * Display a listing of the resource.
     */
    public function index(BlogPostRepository $blogPostRepository)
    {
        $paginator = $blogPostRepository->getAllWithPagination();
        return view('admin.blog.posts.index', compact('paginator'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(BlogCategoryRepository $blogCategoryRepository)
    {
        $item = new BlogPost();
        $categoryList = $blogCategoryRepository->getForComboBox();
        return view('admin.blog.posts.edit', compact('item', 'categoryList'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(BlogPostCreateRequest $request)
    {
        $data = $request->input();
        $item =  BlogPost::query()->create($data);
        if ($item) {
            $job = new BlogPostAfterCreateJob($item);
            $this->dispatch($job);
            return redirect()
                ->route('admin.blog.posts.edit', $item->id)
                ->with(['success' => 'Успешно Сохранен.']);
        } else {
            return back()
                ->withErrors(['msg' => "Ошибка сохранения."])
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     */
    public function show()
    {
        dd(__METHOD__);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id, BlogPostRepository $blogPostRepository, BlogCategoryRepository $blogCategoryRepository)
    {
        if (!$item = $blogPostRepository->getEdit($id)) {
            abort(404);
        }
        if (!$categoryList = $blogCategoryRepository->getForComboBox()) {
            abort(404);
        }
        return view('admin.blog.posts.edit', compact('item', 'categoryList'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(BlogPostUpdateRequest $request, $id, BlogPostRepository $blogPostRepository)
    {
        if (!$item = $blogPostRepository->getEdit($id)) {
            return back()->withErrors(['msg' => "Запись id=[{$id}] не найдена"])->withInput();
        }

        $data = $request->all();

        if ($result = $item->update($data)) {
            return redirect()->route('admin.blog.posts.edit', $item->id)->with(['success' => 'Успешно сохранено']);
        } else {
            return back()->withErrors(['msg' => "Ошибка сохранения."])->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        if ($result = BlogPost::destroy($id)) {
            BlogPostAfterDeleteJob::dispatch($id);

            return redirect()->route('admin.blog.posts.index')->with(['success' => "Запись [$id] удалена"]);
        } else {
            return back()->withErrors(['msg' => "Ошибка удаления."])->withInput();
        }
    }
}
