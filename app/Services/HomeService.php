<?php

namespace App\Services;

class HomeService
{
    /**
     * Pattern list
     * @return \array[][]
     */
    public static function getList()
    {
        $patterns = [
            'Fundamental' => [
                ["title" => "Property container", "url" => route('patterns.property-container')],
                ["title" => "Delegation", "url" => route('patterns.delegation')],
                ["title" => "Event channel", "url" => route('patterns.event-channel')],
                ["title" => "Interface Pattern", "url" => route('patterns.interface')],
            ],
            'Creational' => [
                ["title" => "Abstract factory", "url" => route('patterns.abstract-factory')],
                ["title" => "Factory method", "url" => route('patterns.factory-method')],
                ["title" => "Static factory", "url" => route('patterns.static-factory')],
                ["title" => "Simple factory", "url" => route('patterns.simple-factory')],
                ["title" => "Singleton", "url" => route('patterns.singleton')],
                ["title" => "Multiton", "url" => route('patterns.multiton')],
                ["title" => "Builder", "url" => route('patterns.builder')],
                ["title" => "Lazy initialization", "url" => route('patterns.lazy-initialization')],
                ["title" => "Prototype", "url" => route('patterns.prototype')],
                ["title" => "Object pool", "url" => route('patterns.object-pool')],
            ],
            'Structural' => [
                ["title" => "Adapter", "url" => route('patterns.adapter')],
                ["title" => "Facade", "url" => route('patterns.facade')],
                ["title" => "Bridge", "url" => route('patterns.bridge')],
                ["title" => "Composite", "url" => route('patterns.composite')],
            ],
            'Behavioral' => [
                ["title" => "Strategy", "url" => route('patterns.strategy')],
            ],
        ];

        return $patterns;
    }

    /**
     * Patterns count
     *
     * @return int
     */
    public static function getTotal(): int
    {
        $total = 0;
        foreach (self::getList() as $pattern) {
            $total += count($pattern);
        }
        return $total;
    }
}
