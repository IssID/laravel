<?php

namespace App\Services;

use App\Exceptions\MailException;
use App\Mail\TestMail;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;

class SendMailService
{
    /**
     * @param string $email
     * @return bool
     * @throws MailException
     * @deprecated
     */
    public function sendEmail(string $email)
    {
        $model = new \stdClass();
        $model->text = '<br>Проверка передачи переменной в текст письма';

        try {
            Mail::to($email)->send(new TestMail($model));
        } catch (\Throwable $exception) {
            throw new MailException("Failed to send email");
        }
        return true;
    }

    /**
     * @param string $email
     * @return Mailable
     */
    public function getMail(string $email): Mailable
    {
        $model = new \stdClass();
        $model->text = 'Отправка письма через Job';
        $model->email = $email;

        return new TestMail($model);
    }
}
