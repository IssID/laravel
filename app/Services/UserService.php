<?php

namespace App\Services;

use App\Dto\UserDto;
use App\Exceptions\BusinessException\UserNotFoundException;
use App\Models\User;
use App\Repositories\Api\UserRepository;
use Illuminate\Database\Eloquent\Collection;

class UserService
{
    /** @var UserRepository  */
    public UserRepository $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param UserDto $userDto
     * @return User
     * @throws UserNotFoundException
     */
    public function getUserById(UserDto $userDto): User
    {
        if (!$user = $this->userRepository->find($userDto->id)) {
            throw new UserNotFoundException("User {$userDto->id} not found.", 404);
        }
        return $user;
    }

    /**
     * @return Collection|null
     */
    public function getUsersForExcel(): ?Collection
    {
        return $this->userRepository->getUsersForExcel();
    }
}
