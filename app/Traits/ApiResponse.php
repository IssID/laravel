<?php

namespace App\Traits;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;

trait ApiResponse
{
    /**
     * @param mixed $data
     * @param int $code
     * @return mixed
     */
    protected function success($data = null, int $code = 200)
    {
        $params = [
            'success' => true,
        ];
        if (!empty($data)) {
            if ($data instanceof LengthAwarePaginator) {
                $params['result']['items'] = $data->items();

                $params['result']['pagination']['count'] = $data->count();
                $params['result']['pagination']['current_page'] = $data->currentPage();
                $params['result']['pagination']['last_item'] = $data->lastItem();
                $params['result']['pagination']['last_page'] = $data->lastPage();
                $params['result']['pagination']['per_page'] = $data->perPage();
                $params['result']['pagination']['total'] = $data->total();
            } else {
                $params['result'] = $data;
            }
        }
        return response()->json($params, $code)->getOriginalContent();
    }

    /**
     * @param string|null $message
     * @param int $code
     * @param mixed $data
     * @return JsonResponse
     */
    protected function error(string $message = null, int $code = 400, $data = null): JsonResponse
    {
        $params = [
            'success' => false,
            'code' => $code,
            'message' => $message,
        ];

        if (!empty($data)) {
            $params['data'] = $data;
        }

        return response()->json($params, $code);
    }
}
