<?php

namespace App\Repositories\Api;

use App\Models\User as Model;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class
 * @package App\Repositories
 */
class UserRepository extends BaseRepository
{
    /**
     * @return string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * @param int $userId
     * @return Model|null
     */
    public function find(int $userId): ?Model
    {
        return $this->startConditions()->find($userId);
    }

    /**
     * @return Model[]|Collection|null
     */
    public function getUsersForExcel(): ?Collection
    {
        return $this->startConditions()
            ->select([
                'id',
                'name',
                'email',
                'email_verified_at',
                'updated_at',
                'created_at'
            ])
            ->get();
    }
}
