<?php

namespace App\Factories;

use App\Dto\UserDto;
use App\Exceptions\CreateDtoException;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CreateUserDtoFactory
{
    public static function fromRequest(Request $request): UserDto
    {
        return self::fromArray($request->all());
    }

    public static function fromArray(array $data): UserDto
    {
        $dto = new UserDto();

        try {
            $dto->id = $data['id'] ?? null;
            $dto->name = $data['name'] ?? null;
            $dto->email = $data['email'] ?? null;
            $dto->email_verified_at = isset($data['email_verified_at']) ? (new Carbon($data['email_verified_at'])) : null;
            $dto->password = $data['password'] ?? null;
            $dto->remember_token = $data['remember_token'] ?? null;
            $dto->created_at = isset($data['created_at']) ? (new Carbon($data['created_at'])) : null;
            $dto->updated_at = isset($data['updated_at']) ? (new Carbon($data['updated_at'])) : null;
        } catch (\Throwable $exception) {
            throw new CreateDtoException($exception->getMessage());
        }

        return $dto;
    }
}
