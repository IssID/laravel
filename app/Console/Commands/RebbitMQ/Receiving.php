<?php

namespace App\Console\Commands\RebbitMQ;

use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class Receiving extends Command
{
    protected $signature = 'rabbit:receiving';
    protected $description = 'rabbit Receiving';

    public function handle()
    {
        $connection = new AMQPStreamConnection('rabbitmq', 5672, env("RABBITMQ_USER"), env("RABBITMQ_PASSWORD"));
        $channel = $connection->channel();

        $channel->queue_declare('hello', false, false, false, false);

        $callback = function ($msg) {
            $this->info(' [x] Received ' . $msg->body . "\n");
        };

        $channel->basic_consume('hello', '', false, true, false, false, $callback);

        try {
            $channel->consume();
        } catch (\Throwable $exception) {
            echo $exception->getMessage();
        }
    }
}
