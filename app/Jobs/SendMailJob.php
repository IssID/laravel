<?php

namespace App\Jobs;

use App\Exceptions\MailException;
use App\Mail\TestMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Mailable
     */
    private Mailable $mail;

    /**
     * Create a new job instance.
     *
     * @param Mailable $mail
     */
    public function __construct(Mailable $mail)
    {
        $this->mail = $mail;
    }

    /**
     * @return void
     * @throws MailException
     */
    public function handle()
    {
        try {
            Mail::to($this->mail->data->email)->send($this->mail);
        } catch (\Throwable $exception) {
            throw new MailException("Failed to send email");
        }
    }
}
