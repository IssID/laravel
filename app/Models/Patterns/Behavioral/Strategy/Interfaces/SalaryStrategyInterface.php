<?php

namespace App\Models\Patterns\Behavioral\Strategy\Interfaces;

use App\Models\User;

interface SalaryStrategyInterface
{
    /**
     * @param array $period
     * @param User $user
     * @return int
     */
    public function calc($period, $user): int;

    /**
     * @return string
     */
    public function getName(): string;
}
