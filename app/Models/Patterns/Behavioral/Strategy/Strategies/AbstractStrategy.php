<?php

namespace App\Models\Patterns\Behavioral\Strategy\Strategies;

use App\Models\Patterns\Behavioral\Strategy\Interfaces\SalaryStrategyInterface;

abstract class AbstractStrategy implements SalaryStrategyInterface
{
    /**
     * @param array $period
     * @param \App\Models\User $user
     * @return int
     */
    public function calc($period, $user): int
    {
        return rand(500, 2000);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return class_basename(static::class);
    }
}
