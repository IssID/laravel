<?php

namespace App\Models\Patterns\Behavioral\Strategy;

use App\Models\Patterns\Behavioral\Strategy\Interfaces\SalaryStrategyInterface;
use App\Models\User;
use Illuminate\Support\Collection;

class SalaryManager
{
    /**
     * @var
     */
    private $salaryStrategy;

    /**
     * @var array
     */
    private $period;

    /**
     * @var Collection
     */
    private $users;

    /**
     * SalaryManager constructor.
     * @param array $period
     * @param Collection $users
     */
    public function __construct(array $period, Collection $users)
    {
        $this->period = $period;
        $this->users = $users;
    }

    /**
     * @return Collection
     */
    public function handle()
    {
        $usersSalary = $this->calculateSalary();

        $this->saveSalary($usersSalary);

        return $usersSalary;
    }

    /**
     * @return Collection
     */
    private function calculateSalary()
    {
        $usersSalary = $this->users->map(
            function (User $user) {
                $strategy = $this->getStrategyByUser($user);
                $salary = $this
                    ->setCalculateStrategy($strategy)
                    ->calculateUserSalary($this->period, $user);

                $strategyName = $strategy->getName();
                $userId = $user->id;

                $newItem = compact('userId', 'salary', 'strategyName');

                return $newItem;
            }
        );
        return $usersSalary;
    }

    /**
     * @param User $user
     * @return mixed
     * @throws \Throwable
     */
    private function getStrategyByUser(User $user)
    {
        $strategyName = $user->departmentName() . 'Strategy';
        $strategyClass = __NAMESPACE__ . '\\Strategies\\' . ucwords($strategyName);

        throw_if(!class_exists($strategyClass), \Exception::class,
            "Класс не существует [{$strategyClass}]");

        return new $strategyClass;
    }

    /**
     * @param SalaryStrategyInterface $strategy
     * @return $this
     */
    private function setCalculateStrategy(SalaryStrategyInterface $strategy)
    {
        $this->salaryStrategy = $strategy;
        return $this;
    }

    /**
     * @param $period
     * @param $user
     * @return mixed
     */
    private  function calculateUserSalary($period, $user)
    {
        return $this->salaryStrategy->calc($period, $user);
    }

    /**
     * @param Collection $userSalary
     * @return bool
     */
    private function saveSalary(Collection $usersSalary)
    {
        return true;
    }
}
