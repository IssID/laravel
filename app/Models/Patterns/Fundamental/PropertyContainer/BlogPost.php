<?php


namespace App\Models\Patterns\Fundamental\PropertyContainer;


class BlogPost extends PropertyContainer
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var int
     */
    private $category_id;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getCategory()
    {
        return $this->category_id;
    }

    /**
     * @param $id
     */
    public function setCategory($id)
    {
        $this->category_id = $id;
    }
}
