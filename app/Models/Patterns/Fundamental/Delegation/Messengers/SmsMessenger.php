<?php

namespace App\Models\Patterns\Fundamental\Delegation\Messengers;

class SmsMessenger extends abstractMessenger
{
    /**
     * @return bool
     */
    public function send(): bool
    {
        \Debugbar::info('Sent by ' . __METHOD__);
        return parent::send();
    }
}
