<?php

namespace App\Models\Patterns\Fundamental\Delegation\Messengers;

use App\Models\Patterns\Fundamental\Delegation\Interfaces\MessengerInterface;

abstract class abstractMessenger implements MessengerInterface
{
    /**
     * @var string
     */
    protected $sender;

    /**
     * @var string
     */
    protected $recipient;

    /**
     * @var string
     */
    protected $message;

    /**
     * @param $value
     * @return $this|MessengerInterface
     */
    public function setSender($value): MessengerInterface
    {
        $this->sender = $value;
        return $this;
    }

    /**
     * @param $value
     * @return $this|MessengerInterface
     */
    public function setRecipient($value): MessengerInterface
    {
        $this->recipient = $value;
        return $this;
    }

    /**
     * @param $value
     * @return $this|MessengerInterface
     */
    public function setMessage($value): MessengerInterface
    {
        $this->message = $value;
        return $this;
    }

    /**
     * @return bool
     */
    public function send(): bool
    {
        return true;
    }
}
