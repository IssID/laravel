<?php

namespace App\Models\Patterns\Fundamental\EventChannel\Interfaces;

interface SubscriberInterface
{
    /**
     * Получить имя подписчика
     *
     * @return mixed
     */
    public function getName();

    /**
     * Уведомить подписчика
     *
     * @param $data
     * @return mixed
     */
    public function notify($data);
}
