<?php

namespace App\Models\Patterns\Fundamental\EventChannel\Interfaces;

interface PublisherInterface
{
    /**
     * Уведомление подписчиков
     *
     * @param $data
     * @return mixed
     */
    public function publish($data);
}
