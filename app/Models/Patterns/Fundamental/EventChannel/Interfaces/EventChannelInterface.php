<?php

namespace App\Models\Patterns\Fundamental\EventChannel\Interfaces;

/**
 * Интерфейс канала событий
 * Связующее между подписчиками и издателем
 */
interface EventChannelInterface
{
    /**
     * Издатель уведомляет канал о том что надо всех кто подписан на тему $topic уведомить данными $data
     *
     * @param $topic
     * @param $data
     * @return mixed
     */
    public function publish($topic, $data);

    /**
     * Подписчик $subscriber подписывается на событие $topic
     *
     * @param $topic
     * @param SubscriberInterface $subscriber
     * @return mixed
     */
    public function subscribe($topic, SubscriberInterface $subscriber);
}
