<?php

namespace App\Models\Patterns\Fundamental\EventChannel;

class EventChannelJob
{
    public function run()
    {
        $newsChannel = new EventChannel();

        // Создается поставщик с названием канала
        $highgardenGroup = new Publisher('highgarden-news', $newsChannel);
        $winterfellNews = new Publisher('winterfell-news', $newsChannel);
        $winterfellDaily = new Publisher('winterfell-news', $newsChannel);

        // Создание подписчика
        $sansa = new Subscriber('Sansa Stark');
        $arya = new Subscriber('Arya Stark');
        $cersei = new Subscriber('Sersei Lannister');
        $snow = new Subscriber('Jon Snow');

        // Подписчики подписались на канал
        $newsChannel->subscribe('highgarden-news', $cersei);
        $newsChannel->subscribe('winterfell-news', $sansa);
        $newsChannel->subscribe('highgarden-news', $arya);
        $newsChannel->subscribe('winterfell-news', $arya);
        $newsChannel->subscribe('winterfell-news', $snow);

        // Поставщик публикует данные
        $highgardenGroup->publish("New highgarden post");
        $winterfellNews->publish("New winterfell post");
        $winterfellDaily->publish("Alternative winterfell post");
    }
}
