<?php

namespace App\Models\Patterns\Fundamental\EventChannel;

use App\Models\Patterns\Fundamental\EventChannel\Interfaces\SubscriberInterface;

class Subscriber implements SubscriberInterface
{
    /**
     * @var string
     */
    private $name;

    /**
     * Subscriber constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * Уведомить подписчика
     *
     * @param $data
     * @return mixed|void
     */
    public function notify($data)
    {
        $msg = "{$this->getName()} оповещен данными [{$data}]";
        \Debugbar::info($msg);
    }

    /**
     * Получить имя подписчика
     *
     * @return mixed|string
     */
    public function getName()
    {
        return $this->name;
    }
}
