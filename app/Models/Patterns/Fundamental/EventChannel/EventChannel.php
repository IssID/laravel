<?php

namespace App\Models\Patterns\Fundamental\EventChannel;

use App\Models\Patterns\Fundamental\EventChannel\Interfaces\EventChannelInterface;
use App\Models\Patterns\Fundamental\EventChannel\Interfaces\SubscriberInterface;

/**
 * Канал событий связующее между подписчиками и издателем
 */
class EventChannel implements EventChannelInterface
{
    /**
     * @var array
     */
    private $topics = [];

    /**
     * Подписчик $subscriber подписывается на событие $topic
     *
     * @param $topic
     * @param SubscriberInterface $subscriber
     */
    public function subscribe($topic, SubscriberInterface $subscriber)
    {
        $this->topics[$topic][] = $subscriber;

        $msg = "{$subscriber->getName()} подписался на [{$topic}]";
        \Debugbar::info($msg);
    }

    /**
     * Издатель уведомляет канал о том что надо всех кто подписан на тему $topic уведомить данными $data
     *
     * @param $topic
     * @param $data
     */
    public function publish($topic, $data)
    {
        if (empty($this->topics[$topic])) {
            return;
        }

        foreach ($this->topics[$topic] as $subscriber) {
            $subscriber->notify($data);
        }
    }
}
