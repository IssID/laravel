<?php

namespace App\Models\Patterns\Fundamental\EventChannel;

use App\Models\Patterns\Fundamental\EventChannel\Interfaces\EventChannelInterface;
use App\Models\Patterns\Fundamental\EventChannel\Interfaces\PublisherInterface;

class Publisher implements PublisherInterface
{
    /**
     * @var string
     */
    private $topic;

    /**
     * @var EventChannelInterface
     */
    private $eventChannel;

    /**
     * Publisher constructor.
     *
     * @param $topic
     * @param EventChannelInterface $eventChannel
     */
    public function __construct($topic, EventChannelInterface $eventChannel)
    {
        $this->topic = $topic;
        $this->eventChannel = $eventChannel;
    }

    /**
     * Уведомление подписчиков
     *
     * @param $data
     * @return mixed|void
     */
    public function publish($data)
    {
        $this->eventChannel->publish($this->topic, $data);
    }
}
