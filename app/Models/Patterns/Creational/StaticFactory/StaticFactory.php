<?php

namespace App\Models\Patterns\Creational\StaticFactory;

use App\Models\Patterns\Fundamental\Delegation\AppMessenger;
use App\Models\Patterns\Fundamental\Delegation\Interfaces\MessengerInterface;

class StaticFactory
{
    public static function build(string $type = 'emali'): MessengerInterface
    {
        $messenger = new AppMessenger();
        switch ($type) {
            case 'email':
                $messenger->toEmail();
                $sender = 'admin@test.test';
                break;
            case 'sms':
                $messenger->toSms();
                $sender = 88002000500;
                break;
            default:
                throw new \Exception("Неизвестный тип [{$type}]");
        }

        $messenger
            ->setSender($sender)
            ->setMessage('default Message');

        return $messenger;
    }
}
