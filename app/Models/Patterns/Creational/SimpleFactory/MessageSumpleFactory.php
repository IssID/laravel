<?php

namespace App\Models\Patterns\Creational\SimpleFactory;

use App\Models\Patterns\Fundamental\Delegation\AppMessenger;
use App\Models\Patterns\Fundamental\Delegation\Interfaces\MessengerInterface;

class MessageSumpleFactory
{
    public function build(string $type = 'emali'): MessengerInterface
    {
        $messenger = new AppMessenger();
        switch ($type) {
            case 'email':
                $messenger->toEmail();
                $messenger
                    ->setSender('admin@test.test')
                    ->setMessage('default Message');
                break;
            case 'sms':
                $messenger->toSms();
                $messenger
                    ->setSender(88002000500)
                    ->setMessage('default Message');
                break;
            default:
                throw new \Exception("Неизвестный тип [{$type}]");
        }

        return $messenger;
    }
}
