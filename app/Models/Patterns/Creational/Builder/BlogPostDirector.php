<?php

namespace App\Models\Patterns\Creational\Builder;

use App\Models\Patterns\Creational\Builder\Interfaces\BlogPostBuilderInterface;

class BlogPostDirector
{
    /**
     * @var BlogPostBuilderInterface
     */
    private $builder;

    public function setBuilder(BlogPostBuilderInterface $builder)
    {
        $this->builder = $builder;
        return $this;
    }

    public function createCleanPost()
    {
        $blogPost = $this->builder->getBlogPost();
        return $blogPost;
    }

    public function createNewPostIt()
    {
        $blogPost = $this->builder
            ->setTitle("createNewPostIt")
            ->setBody("createNewPostIt")
            ->setCategories(['category_it'])
            ->setTags(['tag_it', 'tag_new'])
            ->getBlogPost();

        return $blogPost;
    }

    public function createNewPostCats()
    {
        $blogPost = $this->builder
            ->setTitle("createNewPostCats")
            ->setCategories(['category_cats'])
            ->setTags(['tag_cats', 'tag_new'])
            ->getBlogPost();

        return $blogPost;
    }
}
