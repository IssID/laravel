<?php

namespace App\Models\Patterns\Creational\Singleton;

trait SingletonTrait
{
    /**
     * @var self
     */
    private static $instance = null;

    /**
     * @return static
     */
    public static function getInstance()
    {
        return static::$instance ?? (static::$instance = new static());
    }

    /**
     * Запрещаем прямое создание
     */
    private function __constructor()
    {
        //
    }

    /**
     * Запрещаем клонирование
     */
    private function __clone()
    {
        //
    }

    /**
     * Запрещаем десериализацию
     */
    private function __wakeup()
    {
        //
    }
}
