<?php

namespace App\Models\Patterns\Creational\Singleton;

class AdvancedSingleton
{
    use SingletonTrait;

    /**
     * @var mixed
     */
    private $test;

    /**
     * @param $value
     */
    public function setTest($value)
    {
        $this->test = $value;
    }
}
