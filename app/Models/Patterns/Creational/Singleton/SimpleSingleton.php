<?php

namespace App\Models\Patterns\Creational\Singleton;

class SimpleSingleton
{
    /**
     * @var self
     */
    private static $instance;

    /**
     * @var mixed
     */
    private $test;

    /**
     * @return static
     */
    public static function getInstance()
    {
        return static::$instance ?? (static::$instance = new static());
    }

    /**
     * @param $value
     */
    public function setTest($value)
    {
        $this->test = $value;
    }
}
