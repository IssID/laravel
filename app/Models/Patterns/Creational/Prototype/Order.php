<?php

namespace App\Models\Patterns\Creational\Prototype;

use Carbon\Carbon;

class Order
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var Carbon
     */
    public $deliveryDt;

    /**
     * @var Client
     */
    private $client;

    /**
     * Order constructor.
     * @param $id
     * @param Carbon $deliveryDt
     * @param Client $client
     */
    public function __construct($id, Carbon $deliveryDt, Client $client)
    {
        $this->id = $id;
        $this->deliveryDt = $deliveryDt;
        $this->client = $client;
    }

    public function __clone()
    {
        $this->id = $this->id . '_copy';
        $this->deliveryDt = $this->deliveryDt->copy();
        $this->client->addOrder($this);
    }
}
