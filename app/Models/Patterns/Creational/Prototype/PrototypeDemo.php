<?php

namespace App\Models\Patterns\Creational\Prototype;

use Carbon\Carbon;

class PrototypeDemo
{
    public function run()
    {
        $client = new Client(2, 'Клиент');

        $deliveryDt = Carbon::parse('31.12.2030 15:00:00');
        $order = new Order(11, $deliveryDt, $client);

        $client->addOrder($order);

        $clonedOrder = clone $order;
        $clonedOrder->deliveryDt->addDay();

        return compact('client', 'order', 'clonedOrder');
    }
}
