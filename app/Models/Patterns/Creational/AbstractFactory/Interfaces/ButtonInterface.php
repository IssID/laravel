<?php

namespace App\Models\Patterns\Creational\AbstractFactory\Interfaces;

interface ButtonInterface
{
    public function draw();
}
