<?php

namespace App\Models\Patterns\Creational\AbstractFactory\Interfaces;

interface CheckBoxInterface
{
    public function draw();
}
