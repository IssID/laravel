<?php

namespace App\Models\Patterns\Creational\AbstractFactory;

use App\Models\Patterns\Creational\AbstractFactory\Bootstrap\BootstrapFactory;
use App\Models\Patterns\Creational\AbstractFactory\Interfaces\GuiFactoryInterface;
use App\Models\Patterns\Creational\AbstractFactory\Semantic\SemanticUiFactory;

class GuiKitFactory
{
    /**
     * @param $type
     * @return GuiFactoryInterface
     * @throws \Exception
     */
    public function getFactory($type): GuiFactoryInterface
    {
        switch ($type) {
            case 'bootstrap':
                $factory = new BootstrapFactory();
                break;
            case 'semantic':
                $factory = new SemanticUiFactory();
                break;
            default:
                throw new \Exception("Неизвестный тип фабрики [{$type}]");
        }

        return $factory;
    }
}
