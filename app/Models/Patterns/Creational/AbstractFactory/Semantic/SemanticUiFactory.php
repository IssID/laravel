<?php

namespace App\Models\Patterns\Creational\AbstractFactory\Semantic;

use App\Models\Patterns\Creational\AbstractFactory\Interfaces\ButtonInterface;
use App\Models\Patterns\Creational\AbstractFactory\Interfaces\CheckBoxInterface;
use App\Models\Patterns\Creational\AbstractFactory\Interfaces\GuiFactoryInterface;

class SemanticUiFactory implements GuiFactoryInterface
{
    /**
     * @return ButtonInterface
     */
    public function buildButton(): ButtonInterface
    {
        return new ButtonSemanticUi();
    }

    /**
     * @return CheckBoxInterface
     */
    public function buildCheckBox(): CheckBoxInterface
    {
        return new CheckBoxSemanticUi();
    }
}
