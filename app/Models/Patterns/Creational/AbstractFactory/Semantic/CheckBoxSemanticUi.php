<?php

namespace App\Models\Patterns\Creational\AbstractFactory\Semantic;

use App\Models\Patterns\Creational\AbstractFactory\Interfaces\CheckBoxInterface;

class CheckBoxSemanticUi implements CheckBoxInterface
{
    /**
     * @return string
     */
    public function draw()
    {
        return __METHOD__;
    }
}
