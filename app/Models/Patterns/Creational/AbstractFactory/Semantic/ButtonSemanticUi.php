<?php

namespace App\Models\Patterns\Creational\AbstractFactory\Semantic;

use App\Models\Patterns\Creational\AbstractFactory\Interfaces\ButtonInterface;

class ButtonSemanticUi implements ButtonInterface
{
    /**
     * @return string
     */
    public function draw()
    {
        return __METHOD__;
    }
}
