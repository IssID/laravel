<?php

namespace App\Models\Patterns\Creational\AbstractFactory\Bootstrap;

use App\Models\Patterns\Creational\AbstractFactory\Interfaces\ButtonInterface;
use App\Models\Patterns\Creational\AbstractFactory\Interfaces\CheckBoxInterface;
use App\Models\Patterns\Creational\AbstractFactory\Interfaces\GuiFactoryInterface;

/**
 * Фабрика для создания bootstrap
 */
class BootstrapFactory implements GuiFactoryInterface
{
    /**
     * @return ButtonInterface
     */
    public function buildButton(): ButtonInterface
    {
        return new ButtonBootstrap();
    }

    /**
     * @return CheckBoxInterface
     */
    public function buildCheckBox(): CheckBoxInterface
    {
        return new CheckBoxBootstrap();
    }
}
