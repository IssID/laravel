<?php

namespace App\Models\Patterns\Creational\AbstractFactory\Bootstrap;

use App\Models\Patterns\Creational\AbstractFactory\Interfaces\CheckBoxInterface;

/**
 * Реализация Checkbox Bootstrap
 */
class CheckBoxBootstrap implements CheckBoxInterface
{
    /**
     * @return string
     */
    public function draw()
    {
       return __METHOD__;
    }
}
