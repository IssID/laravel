<?php

namespace App\Models\Patterns\Creational\AbstractFactory\Bootstrap;

use App\Models\Patterns\Creational\AbstractFactory\Interfaces\ButtonInterface;

/**
 * Реализация Button Bootstrap
 */
class ButtonBootstrap implements ButtonInterface
{
    /**
     * @return string
     */
    public function draw()
    {
        return __METHOD__;
    }
}
