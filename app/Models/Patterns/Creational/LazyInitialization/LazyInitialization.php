<?php

namespace App\Models\Patterns\Creational\LazyInitialization;

use App\Models\User;

class LazyInitialization
{
    private $user = null;

    public function getUser()
    {
        if (is_null($this->user)) {
            $this->user = User::first();
        }
        return $this->user;
    }
}
