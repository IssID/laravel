<?php

namespace App\Models\Patterns\Creational\Multiton\Interfaces;

interface MultitonInterface
{
    public static function getInstance(string $instanceName): MultitonInterface;
}
