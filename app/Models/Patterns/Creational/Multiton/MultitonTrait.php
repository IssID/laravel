<?php

namespace App\Models\Patterns\Creational\Multiton;

use App\Models\Patterns\Creational\Multiton\Interfaces\MultitonInterface;

trait MultitonTrait
{
    /**
     * @var array
     */
    private static $instances = [];

    /**
     * @var string
     */
    private $name;

    /**
     * @param string $instanceName
     * @return MultitonInterface
     */
    public static function getInstance(string $instanceName): MultitonInterface
    {
        if (isset(static::$instances[$instanceName])) {
            return static::$instances[$instanceName];
        }

        static::$instances[$instanceName] = new static();
        static::$instances[$instanceName]->setName($instanceName);

        return static::$instances[$instanceName];
    }

    /**
     * @param $value
     * @return $this
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }

    /**
     * Запрещаем прямое создание
     */
    private function __constructor()
    {
        //
    }

    /**
     * Запрещаем клонирование
     */
    private function __clone()
    {
        //
    }

    /**
     * Запрещаем десериализацию
     */
    private function __wakeup()
    {
        //
    }
}
