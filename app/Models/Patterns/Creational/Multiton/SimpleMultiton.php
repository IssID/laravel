<?php

namespace App\Models\Patterns\Creational\Multiton;

use App\Models\Patterns\Creational\Multiton\Interfaces\MultitonInterface;

class SimpleMultiton implements MultitonInterface
{
    use MultitonTrait;

    /**
     * @var mixed
     */
    private $test;

    /**
     * @param $value
     */
    public function setTest($value)
    {
        $this->test = $value;
    }
}
