<?php

namespace App\Models\Patterns\Creational\Multiton;

class SimpleMultitonNext extends SimpleMultiton
{
    protected static $instances = [];

    public $test2;
}
