<?php

namespace App\Models\Patterns\Creational\FactoryMethod\Interfaces;

interface ButtonInterface
{
    public function draw();
}
