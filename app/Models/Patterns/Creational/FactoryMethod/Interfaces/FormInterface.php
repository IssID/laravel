<?php

namespace App\Models\Patterns\Creational\FactoryMethod\Interfaces;

interface FormInterface
{
    public function render();
}
