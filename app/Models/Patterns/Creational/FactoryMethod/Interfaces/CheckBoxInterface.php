<?php

namespace App\Models\Patterns\Creational\FactoryMethod\Interfaces;

interface CheckBoxInterface
{
    public function draw();
}
