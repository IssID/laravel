<?php

namespace App\Models\Patterns\Creational\FactoryMethod;

use App\Models\Patterns\Creational\FactoryMethod\Interfaces\FormInterface;
use App\Models\Patterns\Creational\AbstractFactory\Interfaces\GuiFactoryInterface;

abstract class AbstractForm implements FormInterface
{
    /**
     * Отрисовка формы
     *
     * @return mixed
     */
    public function render()
    {
        $guiKit = $this->createGuiKit();
        $result[] = $guiKit->buildButton()->draw();
        $result[] = $guiKit->buildCheckBox()->draw();
        return $result;
    }

    /**
     * Получаем инструментарий для отрисовки объекта
     * @return GuiFactoryInterface
     */
    abstract function createGuiKit(): GuiFactoryInterface;
}
