<?php

namespace App\Models\Patterns\Creational\FactoryMethod;

use App\Models\Patterns\Creational\AbstractFactory\Bootstrap\BootstrapFactory;
use App\Models\Patterns\Creational\AbstractFactory\Interfaces\GuiFactoryInterface;

class BootstrapDialogForm extends AbstractForm
{
    public function createGuiKit(): GuiFactoryInterface
    {
        return new BootstrapFactory();
    }
}
