<?php

namespace App\Models\Patterns\Creational\FactoryMethod;

use App\Models\Patterns\Creational\AbstractFactory\Interfaces\GuiFactoryInterface;
use App\Models\Patterns\Creational\AbstractFactory\Semantic\SemanticUiFactory;

class SemanticUiDialogForm extends AbstractForm
{
    public function createGuiKit(): GuiFactoryInterface
    {
        return new SemanticUiFactory();
    }
}
