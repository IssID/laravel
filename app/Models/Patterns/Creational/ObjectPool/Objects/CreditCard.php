<?php

namespace App\Models\Patterns\Creational\ObjectPool\Objects;

use App\Models\Patterns\Creational\ObjectPool\Interfaces\ObjectPullableInterface;

/**
 * Class CreditCard
 * @package App\Models\Patterns\Creational\ObjectPool\Objects
 *
 * @var string $cardId
 * @var string $cardHolder
 * @var string $cardPwd
 */
class CreditCard implements ObjectPullableInterface
{
    public $cardId;
    public $cardHolder;
    public $cardPwd;
}
