<?php

namespace App\Models\Patterns\Creational\ObjectPool\Objects;

use App\Models\Patterns\Creational\ObjectPool\Interfaces\ObjectPullableInterface;

class User implements ObjectPullableInterface
{
    public $name;
}
