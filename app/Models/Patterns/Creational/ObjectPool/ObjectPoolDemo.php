<?php

namespace App\Models\Patterns\Creational\ObjectPool;

use App\Models\Patterns\Creational\ObjectPool\Objects\Calculator;
use App\Models\Patterns\Creational\ObjectPool\Objects\CreditCard;
use App\Models\Patterns\Creational\ObjectPool\Objects\User;

class ObjectPoolDemo
{

    /**
     * @var ObjectPool
     */
    private $objectPool;

    /**
     * ObjectPoolDemo constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->objectPool = ObjectPool::getInstance();

        $user = new User();
        $creditCard = new CreditCard();
        $calculator = new Calculator();

        $this->objectPool
            ->addObject($user)
            ->addObject($creditCard)
            ->addObject($calculator);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function run()
    {
        if ($creditCard = $this->objectPool->getObject(CreditCard::class)) {
            $creditCard->cardId = '1111 2222 3333 4444';
            $creditCard->cardHolder = 'CARD HOLDER';
            $creditCard->cardPwd = '123';
        }

        if ($user = $this->objectPool->getObject(User::class)) {
            $user->name = 'USER NAME';
        }

        // вернет false запрещаем получать объект если он получен ранее
        if ($user2 = $this->objectPool->getObject(User::class)) {
            $user2->name = 'NEW USER NAME';
        }

        // возвращаем объекты в пул
        $this->objectPool->release($creditCard);
        $this->objectPool->release($user);

        // вернет false запрещаем получать объект если он получен ранее
        if ($user3 = $this->objectPool->getObject(User::class)) {
            $user3->name = 'OLD USER NAME';
        }

        // инициализируем новый объект
        if ($creditCard2 = $this->objectPool->getObject(CreditCard::class)) {
            $creditCard2->cardId = '5555 6666 7777 8888';
            $creditCard2->cardHolder = 'CARD HOLDER X';
            $creditCard2->cardPwd = '321';
        }

        return compact('creditCard', 'creditCard2', 'user', 'user2', 'user3');
    }

}
