<?php

namespace App\Models\Patterns\Structural\Facade;

use App\Models\Patterns\Structural\Facade\Classes\CPU;
use App\Models\Patterns\Structural\Facade\Classes\HardDrive;
use App\Models\Patterns\Structural\Facade\Classes\Memory;

class ComputerFacade
{
    protected $cpu;
    protected $memory;
    protected $hardDrive;

    /**
     * ComputerFacade constructor.
     */
    public function __construct()
    {
        $this->cpu = new CPU();
        $this->memory = new Memory();
        $this->hardDrive = new HardDrive();
    }

    public function startComputer()
    {
        $cpu = $this->cpu;
        $memory = $this->memory;
        $hardDrive = $this->hardDrive;

        $cpu->freeze();
        $memory->load(
            $memory::BOOT_ADDRESS,
            $hardDrive->read($hardDrive::BOOT_SECTOR, $hardDrive::SECTOR_SIZE)
        );
        $cpu->jump($memory::BOOT_ADDRESS);
        $cpu->execute();
    }
}
