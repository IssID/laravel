<?php

namespace App\Models\Patterns\Structural\Facade\Classes;

class HardDrive
{
    const BOOT_SECTOR = 0x001;

    const SECTOR_SIZE = 64;

    public function read($lba, $size) {
        \Debugbar::info(__METHOD__);
        \Debugbar::info($lba);
        \Debugbar::info($size);
        return compact('lba', 'size');
    }
}
