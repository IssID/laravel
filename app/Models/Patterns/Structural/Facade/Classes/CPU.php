<?php

namespace App\Models\Patterns\Structural\Facade\Classes;

class CPU
{
    public function freeze() {
        \Debugbar::info(__METHOD__);
    }

    public function jump($position) {
        \Debugbar::info(__METHOD__);
        \Debugbar::info($position);
    }

    public function execute() {
        \Debugbar::info(__METHOD__);
    }
}
