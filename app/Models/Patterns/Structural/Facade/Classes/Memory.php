<?php

namespace App\Models\Patterns\Structural\Facade\Classes;

class Memory
{
    const BOOT_ADDRESS = 0x0005;
    public function load($position, $data) {
        \Debugbar::info(__METHOD__);
        \Debugbar::info($position);
        \Debugbar::info($data);
    }
}
