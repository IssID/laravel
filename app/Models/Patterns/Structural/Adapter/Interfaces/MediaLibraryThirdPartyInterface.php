<?php

namespace App\Models\Patterns\Structural\Adapter\Interfaces;

interface MediaLibraryThirdPartyInterface
{
    public function addMedia($pathToFile): string;

    public function getMedia($fileCode): string;

    public function newMethod();
}
