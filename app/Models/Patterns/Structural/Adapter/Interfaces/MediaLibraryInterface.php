<?php

namespace App\Models\Patterns\Structural\Adapter\Interfaces;

interface MediaLibraryInterface
{
    public function upload($pathToFile): string;

    public function get($fileCode): string;
}
