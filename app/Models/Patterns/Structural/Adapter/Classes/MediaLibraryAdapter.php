<?php

namespace App\Models\Patterns\Structural\Adapter\Classes;

use App\Models\Patterns\Structural\Adapter\Interfaces\MediaLibraryInterface;

class MediaLibraryAdapter implements MediaLibraryInterface
{
    private $adapterObj;

    /**
     * MediaLibraryAdapter constructor.
     */
    public function __construct()
    {
        $this->adapterObj = new MediaLibraryThirdParty();
    }

    /**
     * @param $pathToFile
     * @return string
     */
    public function upload($pathToFile): string
    {
        return $this->adapterObj->addMedia($pathToFile);
    }

    /**
     * @param $fileCode
     * @return string
     */
    public function get($fileCode): string
    {
        return $this->adapterObj->getMedia($fileCode);
    }

    /**
     * Расширение для получения новых методов адаптора
     *
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws \Exception
     */
    public function __call($name, $arguments)
    {
        if (method_exists($this->adapterObj, $name)) {
            return call_user_func_array([$this->adapterObj, $name], $arguments);
        } else {
            throw new \Exception("Метод {$name} не найден");
        }
    }
}
