<?php

namespace App\Models\Patterns\Structural\Adapter\Classes;

use App\Models\Patterns\Structural\Adapter\Interfaces\MediaLibraryInterface;

class MediaLibrarySelfWritten implements MediaLibraryInterface
{
    /**
     * @param $pathToFile
     * @return string
     */
    public function upload($pathToFile):string
    {
        \Debugbar::info(__METHOD__);

        return md5(__METHOD__ . $pathToFile);
    }

    /**
     * @param $fileCode
     * @return string
     */
    public function get($fileCode): string
    {
        \Debugbar::info(__METHOD__);

        return __METHOD__;
    }
}
