<?php

namespace App\Models\Patterns\Structural\Adapter\Classes;

use App\Models\Patterns\Structural\Adapter\Interfaces\MediaLibraryThirdPartyInterface;

class MediaLibraryThirdParty implements MediaLibraryThirdPartyInterface
{
    /**
     * @param $pathToFile
     * @return string
     */
    public function addMedia($pathToFile): string
    {
        \Debugbar::info(__METHOD__);

        return md5(__METHOD__ . $pathToFile);
    }

    /**
     * @param $fileCode
     * @return string
     */
    public function getMedia($fileCode): string
    {
        \Debugbar::info(__METHOD__);

        return __METHOD__;
    }

    public function newMethod()
    {
        \Debugbar::info(__METHOD__);

        return __METHOD__;
    }

}
