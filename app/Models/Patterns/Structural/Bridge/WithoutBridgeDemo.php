<?php

namespace App\Models\Patterns\Structural\Bridge;

use App\Models\Patterns\Structural\Bridge\Entities\Category;
use App\Models\Patterns\Structural\Bridge\Entities\Product;
use App\Models\Patterns\Structural\Bridge\WithoutBridge\WidgetBigCategory;
use App\Models\Patterns\Structural\Bridge\WithoutBridge\WidgetBigProduct;
use App\Models\Patterns\Structural\Bridge\WithoutBridge\WidgetMiddleCategory;
use App\Models\Patterns\Structural\Bridge\WithoutBridge\WidgetMiddleProduct;
use App\Models\Patterns\Structural\Bridge\WithoutBridge\WidgetSmallCategory;
use App\Models\Patterns\Structural\Bridge\WithoutBridge\WidgetSmallProduct;

class WithoutBridgeDemo
{
    public function run()
    {
        $product = new Product();
        (new WidgetBigProduct())->run($product);
        (new WidgetMiddleProduct())->run($product);
        (new WidgetSmallProduct())->run($product);

        $category = new Category();
        (new WidgetBigCategory())->run($category);
        (new WidgetMiddleCategory())->run($category);
        (new WidgetSmallCategory())->run($category);
    }
}
