<?php

namespace App\Models\Patterns\Structural\Bridge\Entities;

class Product
{
    public $id = 1;

    public $name = 'ProductName';

    public $description = 'ProductDescription';
}
