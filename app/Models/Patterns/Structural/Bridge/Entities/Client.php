<?php

namespace App\Models\Patterns\Structural\Bridge\Entities;

class Client
{
    public $id = 20;

    public $name = 'ClientName';

    public $bio = 'ClientBio';
}
