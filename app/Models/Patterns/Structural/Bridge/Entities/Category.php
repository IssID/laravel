<?php

namespace App\Models\Patterns\Structural\Bridge\Entities;

class Category
{
    public $id = 100;

    public $title = 'CategoryTitle';

    public $description = 'CategoryDescription';
}
