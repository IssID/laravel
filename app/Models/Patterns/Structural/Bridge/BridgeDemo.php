<?php

namespace App\Models\Patterns\Structural\Bridge;

use App\Models\Patterns\Structural\Bridge\Entities\Category;
use App\Models\Patterns\Structural\Bridge\Entities\Product;
use App\Models\Patterns\Structural\Bridge\WithBridge\Abstraction\WingetBigAbstraction;
use App\Models\Patterns\Structural\Bridge\WithBridge\Abstraction\WingetMiddleAbstraction;
use App\Models\Patterns\Structural\Bridge\WithBridge\Abstraction\WingetSmallAbstraction;
use App\Models\Patterns\Structural\Bridge\WithBridge\Realization\CategoryWidgetRealization;
use App\Models\Patterns\Structural\Bridge\WithBridge\Realization\ProductWidgetRealization;

class BridgeDemo
{
    public function run()
    {
        $productRealization = new ProductWidgetRealization(new Product());
        $categoryRealization = new CategoryWidgetRealization(new Category());

        $views = [
          new WingetBigAbstraction(),
          new WingetMiddleAbstraction(),
          new WingetSmallAbstraction(),
        ];

        foreach ($views as $view) {
            $view->run($productRealization);
            $view->run($categoryRealization);
        }
    }
}
