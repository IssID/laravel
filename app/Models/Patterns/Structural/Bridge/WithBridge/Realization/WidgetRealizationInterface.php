<?php

namespace App\Models\Patterns\Structural\Bridge\WithBridge\Realization;

interface WidgetRealizationInterface
{
    public function getId();
    public function getTitle();
    public function getDescription();
}
