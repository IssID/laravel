<?php

namespace App\Models\Patterns\Structural\Bridge\WithBridge\Realization;

use App\Models\Patterns\Structural\Bridge\Entities\Product;

class ProductWidgetRealization implements WidgetRealizationInterface
{
    private $entity;

    public function __construct(Product $product)
    {
        $this->entity = $product;
    }

    public function getId()
    {
        return $this->entity->id;
    }

    public function getTitle()
    {
        return $this->entity->name;
    }

    public function getDescription()
    {
        return $this->entity->description;
    }
}
