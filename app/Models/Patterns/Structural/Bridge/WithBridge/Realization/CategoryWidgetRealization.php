<?php

namespace App\Models\Patterns\Structural\Bridge\WithBridge\Realization;

use App\Models\Patterns\Structural\Bridge\Entities\Category;

class CategoryWidgetRealization  implements WidgetRealizationInterface
{
    private $entity;

    public function __construct(Category $category)
    {
        $this->entity = $category;
    }

    public function getId()
    {
        return $this->entity->id;
    }

    public function getTitle()
    {
        return $this->entity->title;
    }

    public function getDescription()
    {
        return $this->entity->description;
    }
}
