<?php

namespace App\Models\Patterns\Structural\Bridge\WithBridge\Abstraction;

use App\Models\Patterns\Structural\Bridge\WithBridge\Realization\WidgetRealizationInterface;

abstract class AbstractWidget
{
    /**
     * @var
     */
    protected $realization;

    /**
     * @param WidgetRealizationInterface $realization
     */
    public function setRealization(WidgetRealizationInterface $realization)
    {
        $this->realization = $realization;
    }

    /**
     * @return mixed
     */
    public function getRealization()
    {
        return $this->realization;
    }

    /**
     * @param $viewLogic
     */
    protected function viewLogic($viewLogic)
    {
        $method = class_basename( static::class). '::' . __FUNCTION__;
        \Debugbar::info($method);
        \Debugbar::info($viewLogic);
    }
}
