<?php

namespace App\Models\Patterns\Structural\Bridge\WithBridge\Abstraction;

use App\Models\Patterns\Structural\Bridge\WithBridge\Realization\WidgetRealizationInterface;

class WingetBigAbstraction extends AbstractWidget
{
    /**
     * @param WidgetRealizationInterface $realization
     */
    public function run(WidgetRealizationInterface $realization)
    {
        $this->setRealization($realization);

        $viewData = $this->getViewData();
        $this->viewLogic($viewData);
    }

    /**
     * @return array
     */
    private function getViewData()
    {
        $id = $this->getRealization()->getId();
        $fullTItle = $this->getFullTItle();
        $description = $this->getRealization()->getDescription();

        return compact("id", "fullTItle", "description");
    }

    /**
     * @return string
     */
    private function getFullTItle()
    {
        return $this->getRealization()->getId()
            . '::::'
            . $this->getRealization()->getTitle();
    }
}
