<?php

namespace App\Models\Patterns\Structural\Bridge\WithBridge\Abstraction;

use App\Models\Patterns\Structural\Bridge\WithBridge\Realization\WidgetRealizationInterface;

class WingetSmallAbstraction extends AbstractWidget
{
    /**
     * @param WidgetRealizationInterface $realization
     */
    public function run(WidgetRealizationInterface $realization)
    {
        $this->setRealization($realization);

        $viewData = $this->getViewData();
        $this->viewLogic($viewData);
    }

    /**
     * @return array
     */
    private function getViewData()
    {
        $id = $this->getRealization()->getId();
        $smallTItle = $this->getSmallTItle();

        return compact("id", "smallTItle");
    }

    /**
     * @return string
     */
    private function getSmallTItle()
    {
        return \Str::limit($this->getRealization()->getTitle(), 5);
    }
}
