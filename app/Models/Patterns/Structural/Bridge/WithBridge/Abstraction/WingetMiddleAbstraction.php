<?php

namespace App\Models\Patterns\Structural\Bridge\WithBridge\Abstraction;

use App\Models\Patterns\Structural\Bridge\WithBridge\Realization\WidgetRealizationInterface;

class WingetMiddleAbstraction extends AbstractWidget
{
    /**
     * @param WidgetRealizationInterface $realization
     */
    public function run(WidgetRealizationInterface $realization)
    {
        $this->setRealization($realization);

        $viewData = $this->getViewData();
        $this->viewLogic($viewData);
    }

    /**
     * @return array
     */
    private function getViewData()
    {
        $id = $this->getRealization()->getId();
        $middleTItle = $this->getMiddleTItle();
        $description = $this->getRealization()->getDescription();

        return compact("id", "middleTItle", "description");
    }

    /**
     * @return string
     */
    private function getMiddleTItle()
    {
        return $this->getRealization()->getId()
            . '->'
            . $this->getRealization()->getTitle();
    }
}
