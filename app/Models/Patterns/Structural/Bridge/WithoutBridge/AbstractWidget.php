<?php

namespace App\Models\Patterns\Structural\Bridge\WithoutBridge;

use Symfony\Component\VarDumper\VarDumper;

abstract class AbstractWidget
{
    public function viewLogic($viewData)
    {
        $method = class_basename(static::class) . '::' . __FUNCTION__;
        \Debugbar::info($method);
        \Debugbar::info($viewData);
    }
}
