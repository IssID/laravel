<?php

namespace App\Models\Patterns\Structural\Bridge\WithoutBridge;

use App\Models\Patterns\Structural\Bridge\Entities\Product;

class WidgetBigProduct extends AbstractWidget
{
    public function run(Product $product)
    {
        $viewData = $this->getRealizationLogic($product);

        $this->viewLogic($viewData);
    }

    public function getRealizationLogic(Product $product)
    {
        $id = $product->id;
        $fullTitle = $product->id . '::::' . $product->name;
        $description = $product->description;

        return compact('id', 'fullTitle', 'description');
    }
}
