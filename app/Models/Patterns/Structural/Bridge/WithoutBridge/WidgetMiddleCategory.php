<?php

namespace App\Models\Patterns\Structural\Bridge\WithoutBridge;

use App\Models\Patterns\Structural\Bridge\Entities\Category;

class WidgetMiddleCategory extends AbstractWidget
{
    public function run(Category $category)
    {
        $viewData = $this->getRealizationLogic($category);

        $this->viewLogic($viewData);
    }

    public function getRealizationLogic(Category $category)
    {
        $id = $category->id;
        $middleTitle = $category->id . '->' . $category->title;
        $description = $category->description;

        return compact('id', 'middleTitle', 'description');
    }
}
