<?php

namespace App\Models\Patterns\Structural\Bridge\WithoutBridge;

use App\Models\Patterns\Structural\Bridge\Entities\Category;

class WidgetSmallCategory extends AbstractWidget
{
    public function run(Category $category)
    {
        $viewData = $this->getRealizationLogic($category);

        $this->viewLogic($viewData);
    }

    public function getRealizationLogic(Category $category)
    {
        $id = $category->id;
        $middleTitle = \Str::limit($category->title, 5);
        $description = $category->description;

        return compact('id', 'middleTitle', 'description');
    }
}
