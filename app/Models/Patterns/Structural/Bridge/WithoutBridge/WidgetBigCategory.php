<?php

namespace App\Models\Patterns\Structural\Bridge\WithoutBridge;

use App\Models\Patterns\Structural\Bridge\Entities\Category;

class WidgetBigCategory extends AbstractWidget
{
    public function run(Category $category)
    {
        $viewData = $this->getRealizationLogic($category);

        $this->viewLogic($viewData);
    }

    public function getRealizationLogic(Category $category)
    {
        $id = $category->id;
        $fullTitle = $category->id . '::::' . $category->title;
        $description = $category->description;

        return compact('id', 'fullTitle', 'description');
    }
}
