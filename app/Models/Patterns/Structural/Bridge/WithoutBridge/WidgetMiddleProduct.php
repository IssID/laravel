<?php

namespace App\Models\Patterns\Structural\Bridge\WithoutBridge;

use App\Models\Patterns\Structural\Bridge\Entities\Product;

class WidgetMiddleProduct extends AbstractWidget
{
    public function run(Product $product)
    {
        $viewData = $this->getRealizationLogic($product);

        $this->viewLogic($viewData);
    }

    public function getRealizationLogic(Product $product)
    {
        $id = $product->id;
        $middleTitle = $product->id . '->' . $product->name;
        $description = $product->description;

        return compact('id', 'middleTitle', 'description');
    }

}
