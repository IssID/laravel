<?php

namespace App\Models\Patterns\Structural\Bridge\WithoutBridge;

use App\Models\Patterns\Structural\Bridge\Entities\Product;

class WidgetSmallProduct extends AbstractWidget
{
    public function run(Product $product)
    {
        $viewData = $this->getRealizationLogic($product);

        $this->viewLogic($viewData);
    }

    public function getRealizationLogic(Product $product)
    {
        $id = $product->id;
        $smallTitle = \Str::limit($product->name, 7);
        $description = $product->description;

        return compact('id', 'smallTitle', 'description');
    }
}
