<?php

namespace App\Models\Patterns\Structural\Composite\Models;

use App\Models\Patterns\Structural\Composite\Interfaces\CompositeInterface;
use App\Models\Patterns\Structural\Composite\Traits\CompositeTrait;
use Illuminate\Database\Eloquent\Model;

class Order extends Model implements CompositeInterface
{
    use CompositeTrait;

    public $type = 'Заказы';
}
