<?php

namespace App\Models\Patterns\Structural\Composite\Models;

use App\Models\Patterns\Structural\Composite\Interfaces\CompositeItemInterface;
use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model implements CompositeItemInterface
{
    public $type = 'Ингредиент';

    public function calcPrice(): float
    {
        if ($this->price) {
            \Debugbar::debug("[$this->id] {$this->type}::{$this->name} = {$this->price}");
            return $this->price;
        }

        $this->price = \Arr::random([10, 20, 30, 40, 50]);

        \Debugbar::debug("[$this->id] {$this->type}::{$this->name} = {$this->price}");
        return $this->price;
    }
}
