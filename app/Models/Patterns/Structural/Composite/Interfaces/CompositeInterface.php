<?php

namespace App\Models\Patterns\Structural\Composite\Interfaces;

interface CompositeInterface extends CompositeItemInterface
{
    public function setChildItem(CompositeItemInterface $item);
}
