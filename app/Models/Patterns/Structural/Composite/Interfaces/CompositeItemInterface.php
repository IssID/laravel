<?php

namespace App\Models\Patterns\Structural\Composite\Interfaces;

interface CompositeItemInterface
{
    public function calcPrice(): float;
}
