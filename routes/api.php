<?php

use App\Http\Controllers\Api\RabbitController;
use App\Http\Controllers\Api\TestController;
use App\Http\Controllers\Auth\ApiAuthenticatedController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->middleware('api')->group(function () {
    Route::post ('login',   [ApiAuthenticatedController::class, 'login']);
    Route::post ('logout',  [ApiAuthenticatedController::class, 'logout']);
    Route::post ('refresh', [ApiAuthenticatedController::class, 'refresh']);
});

Route::prefix('test')->middleware('auth:api')->group(function () {
    Route::post('/email', [TestController::class, 'sendMailTest']);
    Route::get('/show', [TestController::class, 'getUser']);
});

Route::prefix('rabbit')->middleware('api')->group(function () {
    Route::get('/sending', [RabbitController::class, 'sending']);
    Route::get('/receiving', [RabbitController::class, 'receiving']);
});
