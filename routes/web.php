<?php

use App\Http\Controllers\Admin\Blog\CategoryController as AdminCategoryController;
use App\Http\Controllers\Admin\Blog\PostController as AdminPostController;
use App\Http\Controllers\Admin\Blog\UserController as AdminUserController;
use App\Http\Controllers\Web\Blog\PostController;
use App\Http\Controllers\Web\HomeController;
use App\Http\Controllers\Web\Patterns\BehavioralPatternsController;
use App\Http\Controllers\Web\Patterns\CreationalPatternsController;
use App\Http\Controllers\Web\Patterns\FundamentalPatternsController;
use App\Http\Controllers\Web\Patterns\StructuralPatternsController;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', function () {
    return view('plugin/index');
});

Route::middleware('auth')->group(function () {
    // --- Main Page ---
    Route::get('/home', [HomeController::class, 'index'])->name('home');

    // --- Blog ---
    Route::prefix('blog')->group(function () {
        Route::resource('/posts', PostController::class)
            ->names('blog.posts');
    });

    // --- Admin panel ---
    Route::prefix('admin')->group(function () {
        Route::resource('blog/categories', AdminCategoryController::class)
            ->only(['index', 'edit', 'update', 'create', 'store'])
            ->names('admin.blog.categories');
        Route::resource('blog/posts', AdminPostController::class)
            ->except(['show'])
            ->names('admin.blog.posts');

        Route::get('user/excel', [AdminUserController::class, 'excel'])
            ->name('admin.user.excel');
    });

    // --- Patterns ---
    Route::prefix('patterns')->group(function () {

        // ---------- Fundamental
        Route::get('/property-container', [FundamentalPatternsController::class, 'PropertyContainer'])
            ->name('patterns.property-container');
        Route::get('/delegation', [FundamentalPatternsController::class, 'Delegation'])
            ->name('patterns.delegation');
        Route::get('/event-channel', [FundamentalPatternsController::class, 'EventChannel'])
            ->name('patterns.event-channel');
        Route::get('/interface', [FundamentalPatternsController::class, 'InterfacePattern'])
            ->name('patterns.interface');

        // ---------- Creational
        Route::get('/abstract-factory', [CreationalPatternsController::class, 'AbstractFactory'])
            ->name('patterns.abstract-factory');
        Route::get('/factory-method', [CreationalPatternsController::class, 'FactoryMethod'])
            ->name('patterns.factory-method');
        Route::get('/static-factory', [CreationalPatternsController::class, 'StaticFactory'])
            ->name('patterns.static-factory');
        Route::get('/simple-factory', [CreationalPatternsController::class, 'SimpleFactory'])
            ->name('patterns.simple-factory');
        Route::get('/singleton', [CreationalPatternsController::class, 'Singleton'])
            ->name('patterns.singleton');
        Route::get('/multiton', [CreationalPatternsController::class, 'Multiton'])
            ->name('patterns.multiton');
        Route::get('/builder', [CreationalPatternsController::class, 'Builder'])
            ->name('patterns.builder');
        Route::get('/lazy-initialization', [CreationalPatternsController::class, 'LazyInitialization'])
            ->name('patterns.lazy-initialization');
        Route::get('/prototype', [CreationalPatternsController::class, 'Prototype'])
            ->name('patterns.prototype');
        Route::get('/object-pool', [CreationalPatternsController::class, 'ObjectPool'])
            ->name('patterns.object-pool');

        // ---------- Structural
        Route::get('/adapter', [StructuralPatternsController::class, 'Adapter'])
            ->name('patterns.adapter');
        Route::get('/facade', [StructuralPatternsController::class, 'Facade'])
            ->name('patterns.facade');
        Route::get('/bridge', [StructuralPatternsController::class, 'Bridge'])
            ->name('patterns.bridge');
        Route::get('/composite', [StructuralPatternsController::class, 'Composite'])
            ->name('patterns.composite');

        // ---------- Behavioral
        Route::get('/strategy', [BehavioralPatternsController::class, 'Strategy'])
            ->name('patterns.strategy');
    });
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();
