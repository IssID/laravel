## Делаем код чище
* [Архитектура сложных веб-приложений. С примерами на Laravel](https://github.com/adelf/acwa_book_ru)
* [Соблюдение принципов SOLID при работе с фреймворком Laravel](https://laravel.ru/posts/1427)
* [Data Transfer Object через фабрику без сторонних пакетов](https://youtu.be/lZztzoaEZp4?t=1010)

## Основные команды

Документация по миграции [тут](https://laravel.com/docs/8.x/eloquent)

#### Подготовка
Проверить `.env` если требуется настроить

Установить и запустить `composer` (можно использовать из контейнера `php composer.phar`)
> composer install --ignore-platform-reqs

Устанавливаем алиасы 
> alias sail='bash vendor/bin/sail'

В новом проекте не забыть сгенерировать код продукта
> sail artisan key:generate

Устанавливаем UI (можно из контейнера docker)
>npm install

Генерируем UI
>npm run dev

или
>npm run prod

#### Работа с sail 
(в большенстве случаев можно использовать php artisan из контейнера)

Поднимаем проект в докере можно его пересобрать с флагом `--build`
> sail up -d 

Накатить миграции
> sail artisan migrate --force

Если требуется с начальными данными добавить флаг
> --seed

Для удаления старых данных и заполнения исходными
> sail artisan migrate:refresh --seed

Сгенерить документацию swagger
> sail artisan l5-swagger:generate

Пример создания модели (подробней читать [тут]([https://laravel.com/docs/8.x/eloquent]) )
> sail artisan make:model Flight -m

Откатить миграцию
> sail artisan migrate:rollback --step=1

## Миграции

#### Создание модели и миграции
```
sail artisan make:model BlogCategory -m
sail artisan make:model BlogPost -m
```
#### Создание seeds
```
sail artisan make:seeder UsersTableSeeder
sail artisan make:seeder BlogCategoriesTableSeeder
```
Запуск seeds
```
sail artisan db:seed
sail artisan db:seed --class=UserTableSeeder
sail artisan migrate:refresh --seed
```

## Контроллеры

#### Создание rest контроллера
```
sail artisan make:controller RestTestController --resource
```

#### Контроллеры приложения

Базовый контроллер блога
```
sail artisan make:controller Blog/BaseController
```

Контроллер статей блога
```
sail artisan make:controller Blog/PostController --resource
```

## Валидация данных

Создаем свой `request` для валидации данных
```
sail artisan make:request BlogCategoryUpdateRequest
```
## Очереди (queue jobs)

#### Запуск очереди
```
php artisan queue:work
// Запускает процесс обработки задач как демон.
// Все изменения сделаные в коде после запуска приняты не будут.
// Тоесть после апдейта кода потребуется перезапуск команды.
```
```
php artisan queue:work --queue=queueName1,queueName2
// Сначала выполняет задачи все в очереди queueName1 затем queueName2
```
```
php artisan queue:listen
// Запускает просесс обработки задач указаной очереди.
// Изменения сделаные в коде после запуска будут приняты
// Хуже по производительности чем "queue:work"
```
```
php artisan queue:restart
// мягкий перезапуск queue:work после того как тот завершил выполненую задачу
```
```
php artisan queue:failed
// Просмотреть таблицы проваленых задач
```
```
php artisan queue:retry all
// Возврат в очередь выполнения всех проваленных задач
```
```
php artisan queue:retry 5
// Возврат проваленой задачи (id=5) в очередь выполнения 
```

## adminlte notification
Вывести информацию\ошибку
```
Session::flash('alert-danger', 'danger');
Session::flash('alert-warning', 'warning');
Session::flash('alert-success', 'success');
Session::flash('alert-info', 'info');
```

### JWT auth
[jwt-auth documentation](https://jwt-auth.readthedocs.io/en/develop/)

Добавить метод `unauthenticated` в `app/Exceptions/Handler.php`

Запросы в api делать с хедером `Accept:application/json`
